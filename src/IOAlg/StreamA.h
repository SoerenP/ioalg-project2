#ifndef _IOAlg_StreamA_h
#define _IOAlg_StreamA_h

#include <fcntl.h>
#include <IOAlg/stream.h>
#include <stddef.h>

/* The simple stream that uses the read(), write() system calls. 			*/
/* We should only read "one element at a time". Thus, we set blocksize at creation 	*/
/* and forget about it (will usually be 4 bytes for an integer, depending on usage 	*/
typedef struct InputStreamA {
	const char *filename;
	int handle;
	size_t element_size;
	unsigned int eof : 1;
} InputStreamA;

typedef struct OutputStreamA {
	const char *filename;
	int handle;
	size_t element_size;
	unsigned int eof : 1;
} OutputStreamA;

/* creates the stream. The file will be write only, creation mode */
OutputStreamA *OutputStreamA_create(const char *filename, size_t element_size);

/* returns 1 if stream is at EOF, else 0 */
int InputStreamA_end_of_stream(InputStreamA *st);

/* Opens the stream for read only, so use before reading! */
InputStreamA *InputStreamA_open(const char *filename, size_t element_size);

/* Closes stream (file) after use */
int OutputStreamA_close(OutputStreamA *st);
int InputStreamA_close(InputStreamA *st);


/* After being opened, reads the next (fixed block) of bytes */
int InputStreamA_read_next(InputStreamA *st, void *buffer);

/* After a stream is created, we can write a fixed amount of bytes */
int OutputStreamA_write(OutputStreamA *st, void *buffer);

void StreamA_write_array(OutputStreamA *st, int *A, size_t size);

void StreamA_read_array(InputStreamA *st, int *A, size_t size);

void StreamA_close_input_streams(InputStreamA **streams, int size);

#endif
