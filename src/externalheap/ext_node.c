#include <stdlib.h>
#include <stdio.h>

#include <externalheap/dbg.h>
#include <externalheap/bool.h>
#include <externalheap/ext_node.h>
#include <externalheap/ext_heap.h>
#include <externalheap/meta.h>

struct ext_node *ext_node_create(const char *filename, int num_elements, size_t element_size, enum strategy strat, int B)
{
  check((filename != NULL) && (num_elements >= 0), "Recieved invalid parameters.");

  struct ext_node *new = malloc(sizeof(*new));
  check_mem(new);

  new->num_elements = num_elements;
  new->deleted = 0;
  new->element_size = element_size;
  new->m = ext_meta_create(strat,B);

  /* copy over filename */
  memset(new->filename, '\0',FILENAME_MAX_SIZE);
  strncpy(new->filename,filename,FILENAME_MAX_SIZE);

  return new;
 error:
  return NULL;
}

int ext_node_read_into_rootblock(struct merge_heap *rootblock, struct ext_node *node, int num_elements, int deleted, ext_open open_strat, ext_read read_strat, ext_close close_strat)
{
  FILE *file = (*open_strat)(node->filename, EXT_MODE_R_PLUS, node->m);

  int buf;
  int read = 0;
  int rc = node->element_size; //what it ought to be, unless read fails
  /* first we read past the deleted elements */
  while(read < deleted)
    {
      rc = (*read_strat)(file,1,node->element_size,&buf,node->m);
      if(rc > 0) read++;
      else break;
    }

  check(read == deleted, "Failed to read past first deleted elements, not enough elements in root node");

  /* then we read the next num_elements elements and insert them into the heap for the rootblock */
  read = 0;
  while(read < num_elements && rc > 0)
    {
      rc = (*read_strat)(file,1,node->element_size,&buf,node->m);
      if(rc > 0){
        read++;
        merge_heap_insert(rootblock, item_create(buf,0));
      } else {
        break;
      }
    }
  /* we read "read" elements, not necc num_elements. Return "read" to indicate how many we read */

  /*close the node */
  (*close_strat)(file,node->m);

  return read;
 error:
  return EXT_ERR;
}

void ext_node_write_heap(struct merge_heap *insert_buffer, struct ext_node *node, ext_open open_strat, ext_write write_strat, ext_close close_strat){

  FILE *file = (*open_strat)(node->filename, EXT_MODE_W_PLUS, node->m);
  node->num_elements = 0;
  node->deleted = 0;
  struct item i = merge_heap_delete_min(insert_buffer);
  // set new min for that node
  node->min = i.key;
  while(i.key != -1){
    (*write_strat)(file, 1, sizeof(int), &i.key, node->m);
    node->num_elements++;

    // set new max for that node
    node->max = i.key;
    i = merge_heap_delete_min(insert_buffer);
  }

  (*close_strat)(file, node->m);
}

void ext_node_read_heap(struct merge_heap *merging_buffer, struct ext_node *node, int node_index, int skip, ext_open open_strat, ext_read read_strat, ext_close close_strat)
{
  check(merging_buffer->idx == 0, "merging buffer should be empty before reading entire node into it");

  FILE *file = (*open_strat)(node->filename, EXT_MODE_R_PLUS, node->m);
  int buffer = 0;
  int index = 0;
  int rc = -1;

  // skip the elements in the start
  while(index < skip){
    rc = (*read_strat)(file, 1, sizeof(int), &buffer, node->m);
    check(rc != EXT_ERR, "tried to skip nothing.");
    index++;
  }

  while (1){
    if ((*read_strat)(file, 1, sizeof(int), &buffer, node->m) <= 0)
      break;

    struct item i = item_create(buffer, node_index);
    merge_heap_insert(merging_buffer, i);
  }

 error:
  (*close_strat)(file, node->m);
}

void ext_node_read_and_write_heap(struct merge_heap *merging_buffer,
                                  struct ext_node *child, struct ext_node *parent,
                                  int M,
                                  ext_open open_strat, ext_read read_strat,
                                  ext_write write_strat, ext_close close_strat)
{
  FILE *child_file = (*open_strat)(child->filename, EXT_MODE_R_PLUS, child->m);
  FILE *parent_file = (*open_strat)(parent->filename, EXT_MODE_W_PLUS, parent->m);

  struct item i;
  int buffer = 0;
  int counter = 0;
  int rc = -1;
  debug("we need to skip %d\n",child->deleted);
  // skip stuff in child
  while(counter < child->deleted){
    rc = (*read_strat)(child_file, 1, sizeof(int), &buffer, child->m);
    check(rc != EXT_ERR, "tried to skip nothing.");
    counter++;
  }
  debug("skipped %d in child \n",counter);
  counter = 1;
  // Read first element, insert into heap, deletemin, write to parent.
  (*read_strat)(child_file, 1, sizeof(int), &buffer, child->m);
  merge_heap_insert(merging_buffer, item_create(buffer, 0));
  i = merge_heap_delete_min(merging_buffer);
  (*write_strat)(parent_file, 1, sizeof(int), &i.key, parent->m);
  // Set new min for parent
  parent->min = i.key;

  // Read M elements into parent from child file.
  while (counter < M){
    if ((*read_strat)(child_file, 1, sizeof(int), &buffer, child->m) <= 0){
      break;
    }
    merge_heap_insert(merging_buffer, item_create(buffer, 0));
    i = merge_heap_delete_min(merging_buffer);
    (*write_strat)(parent_file, 1, sizeof(int), &i.key, parent->m);
    parent->max = i.key;
    counter++;
  }
  debug("counter after reading M into parent %d\n",counter);
  debug("child index %d, elemen in bufer %d, deleted %d before close\n",child->m->mn->index,child->m->mn->elements_in_buffer,child->deleted);
  // We have read M or less elements, we should be done.
  (*close_strat)(child_file, child->m);

  // If less than M, write some more to the parent,
  // as there might be elements left in the merge heap.
  while (counter < M){
    i = merge_heap_delete_min(merging_buffer);
    if (i.key == -1){
      break;
    }
    (*write_strat)(parent_file, 1, sizeof(int), &i.key, parent->m);
    parent->max = i.key;
    counter++;
  }
  parent->num_elements = counter;

  debug("merge leaves wrote counter to parent %d, child->elem %d, deleted %d\n",counter,child->num_elements,child->deleted);
  // We have written M elements. close parent.
  (*close_strat)(parent_file, parent->m);

  // Set the minimum key of child
  i = merge_heap_delete_min(merging_buffer);
  // what if the key should be empty?
  if (i.key == -1){
    debug("returning early, child->deleted before %d\, parent del before %d\n",child->deleted,parent->deleted);
    child->deleted = 0;
    parent->deleted = 0;
    child->num_elements = 0;
    child->min = -1;
    child->max = -1;
    debug("returning early \n");
    return;
  }

  // Open child for writing
  child_file = (*open_strat)(child->filename, EXT_MODE_W_PLUS, child->m);
  counter = 0;

  (*write_strat)(child_file, 1, sizeof(int), &i.key, child->m);
  child->min = i.key;
  counter++;

  // Write rest to child
  while (1){
    i = merge_heap_delete_min(merging_buffer);
    if (i.key == -1){
      break;
    }
    (*write_strat)(child_file, 1, sizeof(int), &i.key, child->m);
    child->max = i.key;

    counter++;
  }
  child->num_elements = counter;
  debug("merge leaves wrote counter to child %d\n",counter);
  // since we have made fresh files, reset deleted.
  child->deleted = 0;
  parent->deleted = 0;

  // We have written counter elements, close child
  (*close_strat)(child_file, child->m);
  return;

 error:
  (*close_strat)(child_file, child->m);
  return;
}

int ext_node_destroy(struct ext_node *node)
{
  check(node != NULL, "Recieved NULL pointer.");

  if(node->m) ext_meta_destroy(node->m);

  free(node);

  node = NULL;

  return EXT_OK;
 error:
  return EXT_ERR;
}

void print_node(struct ext_node *node){
  fprintf(stderr,"Printing Node:       \n \t num_elements: %d, \n \t deleted:      %d, \n\t max:          %d, \n\t min:          %d, \n\t filename:     %s  \n",
          node->num_elements,
          node->deleted,
          node->max,
          node->min,
          node->filename);
}
