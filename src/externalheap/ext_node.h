#ifndef __EXT_NODE_H_
#define __EXT_NODE_H_

#include <stdio.h> // FILE dependency
#include <externalheap/meta.h>
#include <externalheap/strategies.h>
#include <externalheap/merge_heap.h>

#define FILENAME_MAX_SIZE 128

struct ext_node {
  int num_elements; //between m/2 and m, if not rightmost leaf
  int deleted; //how many of the first elements should be skipped / deleted
  size_t element_size;
  int max; // the biggest element
  int min; // the smallest element
  char filename[FILENAME_MAX_SIZE];
  struct meta *m; 
};

struct ext_node *ext_node_create(const char *filename, int num_elements, size_t element_size, enum strategy strat, int B);

void ext_node_write_heap(struct merge_heap *insert_buffer, struct ext_node *node, ext_open open_strat, ext_write write_strat, ext_close close_strat);

void ext_node_read_heap(struct merge_heap *merging_buffer, struct ext_node *node, int node_index, int skip, ext_open open_strat, ext_read read_strat, ext_close close_strat);

void ext_node_read_and_write_heap(struct merge_heap *merging_buffer, struct ext_node *child, struct ext_node *parent, int M, ext_open open_strat, ext_read read_strat, ext_write write_strat, ext_close close_strat);

int ext_node_read_into_rootblock(struct merge_heap *rootblock, struct ext_node *node, int num_elements, int deleted, ext_open open_strat, ext_read read_strat, ext_close close_strat);

int ext_node_destroy(struct ext_node *node);

void print_node(struct ext_node *node);

#endif //__EXT_NODE_H_
