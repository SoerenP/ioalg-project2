#include <stdlib.h>

#include <externalheap/bool.h>
#include <externalheap/merge_heap.h>
#include <externalheap/dbg.h>

static int double_the_space(struct merge_heap *h){
  struct item *temp = realloc(h->elements, 2*h->maxsize*sizeof(struct item));
  check_mem(temp);

  h->elements = temp;
  h->maxsize = h->maxsize*2;

  return MERGE_HEAP_OK;
 error:
  return MERGE_HEAP_ERR;
}

static bool is_full(struct merge_heap *h){
  if (h->idx >= h->maxsize)
    return TRUE;
  return FALSE;
}

static bool is_empty(struct merge_heap *h){
  if (h->idx <= 0)
    return TRUE;
  return FALSE;
}

static void swap(struct merge_heap *h, int i, int j) {
  struct item temp = h->elements[i];
  h->elements[i] = h->elements[j];
  h->elements[j] = temp;
}


// Heap implementation
struct merge_heap *merge_heap_create(int maxsize) {
  check(maxsize > 0, "size of heap must be positive");
  struct merge_heap *h = malloc(sizeof(*h));
  check_mem(h);
  h->idx = 0;
  h->maxsize = maxsize;
  h->elements = malloc(h->maxsize * sizeof(struct item));
  check_mem(h->elements);

  return h;
 error:
  return NULL;
}

struct item item_create(int key, int list){
  struct item i;
  i.key = key;
  i.list = list;
  return i;
}

void merge_heap_destroy(struct merge_heap *h){
  free(h->elements);
  free(h);
}

int merge_heap_insert(struct merge_heap *h, struct item i) {
  if (is_full(h) == TRUE){
    check(double_the_space(h) == MERGE_HEAP_OK, "Could not double the space of array.");
  }
  h->elements[h->idx] = i;
  h->idx++;

  int inserted = h->idx - 1;
  int indexOfParent = (inserted + 1) / 2 - 1;
  while (0 < inserted &&
         h->elements[inserted].key < h->elements[indexOfParent].key) {
    swap(h, inserted, indexOfParent);
    inserted = indexOfParent;
    indexOfParent = (inserted + 1) / 2 - 1;
  }
  return MERGE_HEAP_OK;
  
 error:
  return MERGE_HEAP_ERR;
}

struct item merge_heap_delete_min(struct merge_heap *h) {
  if (is_empty(h) == TRUE){
    struct item i;
    i.key = MERGE_HEAP_ERR;
    i.list = MERGE_HEAP_ERR;
    return i;
  }

  struct item ret = h->elements[0];
  swap(h, 0, h->idx - 1);
  h->idx--;

  int size = h->idx;
  int nodeToBeFixed = 0;
  int childL = (nodeToBeFixed + 1) * 2 - 1;
  int childR = (nodeToBeFixed + 1) * 2;

  while (TRUE) {
    // If nodeToBeFixed doesn't have any children.
    if (size <= childL) break;

    // If nodeToBeFixed only has a left child.
    if (size == childR) {
      if (h->elements[childL].key < h->elements[nodeToBeFixed].key){
        swap(h,nodeToBeFixed, childL);
      }
      break;
    }

    // If nodeToBeFixed has both children.
    // Find the smallest of the two children
    if (h->elements[childL].key < h->elements[childR].key &&
        h->elements[childL].key < h->elements[nodeToBeFixed].key) {
      swap(h, nodeToBeFixed, childL);
      nodeToBeFixed = childL;
      childL = (nodeToBeFixed + 1) * 2 - 1;
      childR = (nodeToBeFixed + 1) * 2;
    }
    // We know that the left child couldn't have been the smallest node of the three.
    // So, we test whether the right child is smaller than nodeToBeFixed.
    else if (h->elements[childR].key < h->elements[nodeToBeFixed].key) {
      swap(h, nodeToBeFixed, childR);
      nodeToBeFixed = childR;
      childL = (nodeToBeFixed + 1) * 2 - 1;
      childR = (nodeToBeFixed + 1) * 2;
    } else {
      break;
    }
  }

  return ret;
}

/* overrides the input array to be sorted */
void merge_heap_heapsort_inplace(struct item *A, int size)
{
  struct merge_heap *h = merge_heap_create(size);
  check_mem(h);

  int index = 0;
  for(index = 0; index < size; index++)
    {
      merge_heap_insert(h, A[index]);
    }

  for(index = 0; index < size; index++)
    {
      A[index] = merge_heap_delete_min(h);
    }

  if(h) merge_heap_destroy(h);
 error:
  return;
}

struct item *merge_heap_heapsort(struct item *A, int size)
{
  struct merge_heap *h = merge_heap_create(size);
  check_mem(h);

  struct item *sorted = malloc(sizeof(struct item) * size);
  check_mem(sorted);

  int index = 0;
  for(index = 0; index < size; index++)
    {
      merge_heap_insert(h,A[index]);
    }

  for(index = 0; index < size; index++)
    {
      sorted[index] = merge_heap_delete_min(h);
    }

  if(h) merge_heap_destroy(h);
  return sorted;
 error:
  if(h) merge_heap_destroy(h);
  return NULL;
}
