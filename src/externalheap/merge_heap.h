#ifndef __MERGE_HEAP_H_
#define __MERGE_HEAP_H_

struct item {
  int key; // key of the thing we insert
  int list; // list number where it came from
};

struct merge_heap {
  int idx;
  struct item *elements;
  int maxsize;
};

#define MERGE_HEAP_OK 0
#define MERGE_HEAP_ERR (-1)

struct merge_heap *merge_heap_create(int maxsize);
struct item item_create(int key, int list);

int merge_heap_insert(struct merge_heap *h, struct item i);
struct item merge_heap_delete_min(struct merge_heap *h);
void merge_heap_destroy(struct merge_heap *h);

struct item *merge_heap_heapsort(struct item *A, int size);
void merge_heap_heapsort_inplace(struct item *A, int size);

#endif
