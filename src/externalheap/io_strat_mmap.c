#include <externalheap/io_strat_mmap.h>
#include <externalheap/dbg.h>

#include <fcntl.h> //files
#include <stdlib.h>
#include <assert.h>

#include <unistd.h> // open, read, getpagesize
#include <stddef.h> // size_t
#include <sys/stat.h> //page size
#include <sys/mman.h> //mmap

static int stretch_file(FILE *file, struct meta *m){
  // get file handle from file
  int fd = fileno(file);

  // figure out how much more "offset" we need
  int offset = m->mm->block_size - (m->mm->file_size % m->mm->block_size);

  // stretch the file to match page sizes, such that we can read
  // -1, because we want to be just before the end
  debug( "Stretching file to be size %zu.\n", m->mm->file_size + offset);
  int eof = lseek(fd, m->mm->file_size + offset - 1, SEEK_SET);
  check(eof != -1, "Could not go to end of file.");

  eof = write(fd, "", 1);
  check(eof == 1, "Could not write to the end of the file.");

  // go to the start of the file again.
  lseek(fd, 0, SEEK_SET);

  return 0;
 error:
  return -1;
}

static int stretch_back_file(FILE *file, struct meta *m){
  // get file handle from file
  int fd = fileno(file);

  debug( "Truncating the file to size %zu\n", m->mm->file_size);
  check(0 == ftruncate(fd, m->mm->file_size), "Could not truncate file back to original size.");

  return 0;
 error:
  return -1;
}

static void read_in_new_map(FILE *file, struct meta *m){
  debug( "Reading in new map. Offset: %d, Current pos: %zu \n", m->mm->offset, m->mm->curr_pos);
  size_t length = m->mm->block_size;
  size_t offset = m->mm->offset * length;

  m->mm->curr_map = (int*) mmap(0, length, PROT_READ | PROT_WRITE, MAP_SHARED, fileno(file), offset);
  check(m->mm->curr_map != MAP_FAILED, "Could not map the memory.");

  // Stretch the file to make room for a new map.
  stretch_file(file, m);
 error:
  return;
}

static void release_map(struct meta *m){
  size_t length = m->mm->block_size;
  check(munmap(m->mm->curr_map, length) != -1, "Could not unmap the memory.");
  m->mm->curr_map = NULL;
  m->mm->offset++; // count up the offset if we are to read in more stuff
 error:
  return;
}

static void reset_meta(struct meta *m){
  m->mm->file_size = 0;
  m->mm->curr_map = NULL;
  m->mm->offset = 0;
  m->mm->curr_pos = 0;
}

FILE *io_strat_mmap_open(const char *file_name, const char *mode, struct meta *m){
  check((file_name != NULL) && (mode != NULL) && (m != NULL), "Received NULL pointer.");
  check(m->s == MMAP, "Wrong meta for mmap.");

  FILE *file = fopen(file_name, mode);
  check(file != NULL, "Failed to open file.");

  reset_meta(m);

  struct stat fstat;
  check( stat(file_name, &fstat) >= 0, "Error reading filesize of file. Filename was %s.", file_name); 
  m->mm->file_size = fstat.st_size;

  return file;
 error:
  return NULL;
}

int io_strat_mmap_read(FILE* file, int number_of_elements, size_t element_size, int *buffer, struct meta *m){
  int elements = m->mm->block_size / element_size;
  int index = 0;
  
  while (index < number_of_elements){
    // we exceed the file?
    if (m->mm->curr_pos * element_size >= m->mm->file_size){
      debug( "End of file.\n");
      return 0; // we can't read any more elements, onegai sensei
    }
    // Our current position in the page
    unsigned int pos_map = m->mm->curr_pos % elements;
  
    // Reached end of block, or start of map
    if (pos_map == 0){
      // End of block
      if (m->mm->curr_map != NULL)
        release_map(m);
      
      read_in_new_map(file, m);
    }
  
    memcpy(&buffer[index], &(m->mm->curr_map[pos_map]), element_size);

    m->mm->curr_pos++;
    index++;
  }
  
  return element_size * number_of_elements;
}

int io_strat_mmap_write(FILE* file, int number_of_elements, size_t element_size, int *buffer, struct meta *m){
  // I want to calculate in bytes.
  // So size of array is transformed into the bytes by using the element size.
  // Blocksize should be in bytes already
  off_t old_size = m->mm->file_size;
  m->mm->file_size += number_of_elements*element_size;
  
  debug( "old size: %zu, new size: %zu \n", old_size, m->mm->file_size);
  // Stretch the file so we can map mmap elements to it
  // Go to the place were we want to append
  // does this matter?
  lseek(fileno(file), old_size, SEEK_SET); //TODO check this for off by 1

  // first map
  if (m->mm->curr_map == NULL)
    read_in_new_map(file, m);

  // Time to map the file. We can fit in blocksize bytes, but we
  // use elementsize for each index, meaning the total assignments that
  // can be done is blocksize/elementsize
  int index = 0;
  while (m->mm->curr_pos < (m->mm->file_size / element_size)
         && (m->mm->curr_pos) < (number_of_elements + old_size/element_size)) {
    // If we exceed the map, write it and map a new one!
    if (m->mm->curr_pos >
        (m->mm->offset + 1) * (m->mm->block_size / element_size) - 1){

      debug( "Reading in new map, since currentpos: %zu \n", m->mm->curr_pos);
      // Sync mmap to disk
      int err = msync(m->mm->curr_map, m->mm->block_size, MS_SYNC);
      check(err != -1, "Could not sync from map to disk.");

      if (m->mm->curr_map != NULL){
        release_map(m);
      }

      read_in_new_map(file, m);
    }
    debug( "writing %d, currentpos: %zu \n",buffer[index], m->mm->curr_pos);
    m->mm->curr_map[m->mm->curr_pos % (m->mm->block_size / element_size)] = buffer[index];
    m->mm->curr_pos++;
    index++;
  }

  return 0;
 error:
  return -1;
}

int io_strat_mmap_close(FILE* file, struct meta *m){
  if (m->mm->curr_map != NULL) {
    // Sync the last part to the disk.
    int err = msync(m->mm->curr_map, (m->mm->curr_pos * sizeof(int)) % m->mm->block_size, MS_SYNC);
    check(err != -1, "Could not write from map to disk");
    release_map(m);
  }
  
  stretch_back_file(file, m);

  int rc = fclose(file);
  check(rc != -1, "Could not close file");

  return 0;
 error:
  return -1;
}
