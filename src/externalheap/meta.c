#include <stdlib.h>
#include <stdio.h>

#include <externalheap/ext_heap.h>
#include <externalheap/meta.h>
#include <externalheap/dbg.h>
#include <externalheap/bool.h>

int ext_meta_reset(struct meta *m)
{
  check(m != NULL, "Recieved NULL pointer.");

  switch(m->s){
  case NAIVE:
    /* memset buffer to 0 */
    memset(m->mn->buffer,0,m->mn->blocksize * sizeof(int));
    m->mn->elements_in_buffer = 0;
    m->mn->index = 0;
    m->mn->eof = FALSE;
    m->mn->opened_for_writing = FALSE;
    break;
  case STD:
    //TODO
    break;
  case MMAP:
    //TODO
    break;
  }

  return EXT_OK;
 error:
  return EXT_ERR;
}


int ext_meta_destroy(struct meta *m)
{
  check(m != NULL, "Recieved NULL pointer.");

  switch(m->s){
  case NAIVE:
    if(m->mn){
      if(m->mn->buffer) free(m->mn->buffer);
      free(m->mn);
    }
    break;
  case STD:
    if(m->ms) free(m->ms);
    break;
  case MMAP:
    if(m->mm) free(m->mm);
    break;
  }

  free(m);
  return EXT_OK;
 error:
  return EXT_ERR;
}

struct meta *ext_meta_create(enum strategy strat, int B){
  struct meta *m = malloc(sizeof(*m));
  check_mem(m);
  m->s = strat;

  switch(strat){
  case NAIVE:
    {
      struct meta_naive *mn = malloc(sizeof(*mn));
      check_mem(mn);

      mn->blocksize = B;
      mn->index = 0;
      mn->elements_in_buffer = 0;
      mn->eof = FALSE;
      mn->opened_for_writing = FALSE;
      mn->buffer = malloc(sizeof(int) * mn->blocksize);
      check_mem(mn->buffer);
      m->mn = mn;
      return m;
      break;
    }
  case STD:
    {
      struct meta_std *ms = malloc(sizeof(*ms));
      check_mem(ms);

      ms->blocksize = B;
      m->ms = ms;
      
      return m;
      break;
    }
  case MMAP:
    {
      struct meta_mmap *mm = malloc(sizeof(*mm));
      check_mem(mm);
      
      mm->file_size = 0;
      mm->curr_map = NULL;
      mm->offset = 0;
      mm->curr_pos = 0;
      mm->block_size = B;
      m->mm = mm;
      
      return m;
      break;
    }
  default:
    return NULL;
  }
    
 error:
  return NULL;
}
