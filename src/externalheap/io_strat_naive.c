#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#include <externalheap/dbg.h>
#include <externalheap/io_strat_naive.h>
#include <externalheap/bool.h>

static int file_to_buffer(FILE *file, int num_elements, size_t element_size, struct meta *m)
{
  int fd = fileno(file);
  check(fd != EXT_ERR, "Failed to get file descriptor from file pointer.");
  int rc = read(fd,m->mn->buffer,num_elements * element_size);
  check(rc >= 0, "Failed to read correct amount");
  m->mn->index = 0;
  m->mn->elements_in_buffer = rc / element_size;
  if(rc <= 0){
    m->mn->eof = TRUE;
  } else {
    m->mn->eof = FALSE;
  }

  return rc;
 error:
  return EXT_ERR;
}

static int buffer_to_file(FILE *file, int num_elements, size_t element_size, struct meta *m)
{
  int fd = fileno(file);
  check(fd != EXT_ERR, "Failed to get file descriptor from file pointer.");

  int rc = write(fd,m->mn->buffer,num_elements * element_size);
  check(rc >= 0, "Failed to write entire buffer.");
  memset(m->mn->buffer,0,m->mn->blocksize * element_size);
  m->mn->index = 0;

  return rc;
 error:
  return EXT_ERR;
}

int io_strat_naive_read(FILE *file, int num_elements, size_t element_size, int *buffer, struct meta *m)
{
  check((file != NULL) && (buffer != NULL) && (m != NULL) && (m->s == NAIVE),
	"Recieved invalid arguments");

  check(num_elements <= m->mn->blocksize - m->mn->index, "Requested more elements than buffer can contain");

  if(m->mn->eof == TRUE) return EXT_EOF;

  int bytes_to_read = num_elements * element_size;
  memcpy(buffer,&m->mn->buffer[m->mn->index],bytes_to_read);
  m->mn->index += num_elements;
  if(m->mn->index >= m->mn->blocksize || m->mn->index >= m->mn->elements_in_buffer)
    {
      m->mn->eof = TRUE; //we might be eof until proven otherwise
      int rc = file_to_buffer(file,m->mn->blocksize,element_size,m);
      check(rc != EXT_ERR, "Failed to read from file to buffer.");
    }

  return bytes_to_read;
 error:
  return EXT_ERR;
}

int io_strat_naive_write(FILE *file, int num_elements, size_t element_size, int *buffer, struct meta *m)
{
  check((file != NULL) && (buffer != NULL) && (m != NULL) && (m->s == NAIVE),
	"Recieved invalid arguments..");

  check(num_elements <= m->mn->blocksize, "Requested to write more than buffer can contain.");

  int rc = 0;
  /* if index at end, dump the buffer */
  if(m->mn->index >= m->mn->blocksize)
    {
      rc =  buffer_to_file(file, m->mn->blocksize, element_size, m);
      check(rc != EXT_ERR, "Failed to dump buffer to file");
    }

  /* copy data over into buffer */
  int bytes_to_write = num_elements * element_size;
  memcpy(&m->mn->buffer[m->mn->index],buffer, bytes_to_write);
  m->mn->index += num_elements;

  return bytes_to_write;
 error:
  return EXT_ERR;
}

FILE *io_strat_naive_open(const char *filename, const char *mode, struct meta *m)
{
  check((filename != NULL) && (mode != NULL) && (m != NULL), "Recieved NULL pointer.");

  FILE *file = fopen(filename, mode);
  check(file != NULL, "Failed to open file.");

  ext_meta_reset(m);

  /* if mode is read, read the first B elements into buffer */
  if((0 == strcmp(mode,EXT_MODE_R)) || (0 == strcmp(mode,EXT_MODE_R_PLUS))){
    m->mn->opened_for_writing = FALSE;
    int rc = file_to_buffer(file,m->mn->blocksize,sizeof(int),m);
    check(rc != EXT_ERR, "Failed to read file into buffer.");
  } else {
    m->mn->opened_for_writing = TRUE;
  }

  return file;
 error:
  return NULL;
}

int io_strat_naive_close(FILE *file, struct meta *m)
{
  check((file != NULL) && (m != NULL), "Recieved NULL pointer.");
  /* check if still elements in buffer, if so, write them to file, but only if we opened for writing */
  if((m->mn->index > 0) && (m->mn->opened_for_writing == TRUE)) buffer_to_file(file,m->mn->index,sizeof(int),m);

  int rc = fclose(file);
  check(rc != EOF, "Failed to close file.");

  ext_meta_reset(m);

  file = NULL;

  return EXT_OK;
 error:
  return EXT_ERR;
}

