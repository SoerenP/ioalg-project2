#ifndef __META_H_
#define __META_H_

#include <stddef.h>

struct meta_naive {
  int blocksize;
  int elements_in_buffer;
  int *buffer;
  int index;
  int eof;
  int opened_for_writing;
};

struct meta_std {
  int blocksize;
};

struct meta_mmap {
  size_t file_size; // in bytes
  int* curr_map; // contains block_size/sizeof(int) elements
  unsigned int offset; 
  size_t curr_pos; // in elements
  size_t block_size; // in bytes
};

enum strategy { NAIVE, STD, MMAP };

struct meta {
  enum strategy s;
  union {
    struct meta_naive *mn;
    struct meta_std   *ms;
    struct meta_mmap  *mm;
  };
};

int ext_meta_reset(struct meta *m);
struct meta *ext_meta_create(enum strategy strat, int B);
int ext_meta_destroy(struct meta *m);

#endif
