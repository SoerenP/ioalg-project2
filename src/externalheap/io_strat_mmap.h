#ifndef __IO_STRAT_MMAP_H
#define __IO_STRAT_MMAP_H

#include <stdio.h>
#include <externalheap/meta.h>

int io_strat_mmap_read(FILE* file, int number_of_elements, size_t element_size, int *buffer, struct meta *m);
int io_strat_mmap_write(FILE* file, int number_of_elements, size_t element_size, int *buffer, struct meta *m);
FILE *io_strat_mmap_open(const char *file_name, const char *mode, struct meta *m);
int io_strat_mmap_close(FILE* file, struct meta *m);

#endif // __IO_STRAT_MMAP_H
