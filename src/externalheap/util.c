#include <stdio.h>

#include <externalheap/util.h>


void next_file_name(char *buffer, size_t buffer_size, const char *prefix, bool reset)
{
  static int post_fix = 0;
  if(reset == TRUE){
    post_fix = 0;
  }

  memset(buffer,0,buffer_size);

  snprintf(buffer,
	   sizeof(char)*buffer_size,
	   "%s%d",
	   prefix,
	   post_fix);

  post_fix++;
}
