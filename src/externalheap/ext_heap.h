#ifndef __EXT_HEAP_H_
#define __EXT_HEAP_H_

#define EXT_OK 0
#define EXT_ERR (-1)
#define EXT_EOF 0

#define EXT_MODE_R "r"
#define EXT_MODE_R_PLUS "r+"
#define EXT_MODE_W "w"
#define EXT_MODE_W_PLUS "w+"
#define EXT_MODE_A "a"
#define EXT_MODE_A_PLUS "a+"

#include <stdio.h> // FILE
#include <externalheap/meta.h>
#include <externalheap/ext_node.h>
#include <externalheap/strategies.h>
#include <externalheap/merge_heap.h>

struct ext_heap {
  struct ext_node **nodes; // The array of nodes in heap order
  int max_size; // Amount allocated
  int size; // The amount of nodes
  int M; // Size M - for amount of elements in nodes.
  int B; // Block size 
  int threshold;
  struct merge_heap *root_min_elements;
  struct merge_heap *insert_buffer;
  struct merge_heap *merging_buffer;
  enum strategy strat;

  // Function pointers go here, depending on the strategy. 
  ext_write write;
  ext_open open;
  ext_read read;
  ext_close close;
};

struct ext_heap *ext_heap_create(int max_size, int M, int B, enum strategy strat, ext_write w, ext_open o, ext_read r, ext_close c);
void ext_heap_destroy(struct ext_heap *heap);
int ext_heap_insert(struct ext_heap *heap, int i);
int ext_heap_delete_min(struct ext_heap *heap);
int heap_child(int parent_index, int fanout, int c);
int heap_parent(int child_index, int fanout);
int count_elements_in_heap(struct ext_heap *Q);
int ext_heap_sort(struct ext_node *input_file, struct ext_node *output_file, int N, int M, int B, enum strategy strat, ext_write w, ext_open o, ext_read r, ext_close c );

void printfile(const char *name, int B);

#endif // __EXT_HEAP_H_
