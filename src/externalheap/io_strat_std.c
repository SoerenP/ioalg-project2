#include <stdlib.h>
#include <externalheap/dbg.h>
#include <externalheap/io_strat_std.h>

int io_strat_std_read
(FILE *file, int num_elements, size_t element_size, int *buffer, struct meta *m)
{
  check((file != NULL) && (buffer != NULL) && (m != NULL) && (m->s == STD),
	"Recieved invalid arguments");

  int rc = fread(buffer,element_size,num_elements,file);
  check(rc >= 0, "Failed to read anything.");

  return rc;
 error:
  return EXT_ERR;
}

int io_strat_std_write
(FILE *file, int num_elements, size_t element_size, int *buffer, struct meta *m)
{
  check((file != NULL) && (buffer != NULL) && (m != NULL) && (m->s == STD),
	"Recieved invalid arguments");

  int rc = fwrite(buffer,element_size,num_elements,file);
  check(rc >= 0, "Failed to write anything.");

  return rc;
 error:
  return EXT_ERR;
}

FILE *io_strat_std_open
(const char *filename, const char *mode, struct meta *m)
{
  check((filename != NULL) && (mode != NULL) && (m != NULL), 		"Recieved NULL pointer.");

  FILE *file = fopen(filename, mode);
  check(file != NULL, "Failed to open file. Name was %s, mode was %s", filename, mode);

  return file;
 error:
  return NULL;
}

int io_strat_std_close(FILE *file, struct meta *m)
{
  check((file != NULL) && (m != NULL), "Recieved NULL pointer.");

  int rc = fclose(file);
  check(rc != EOF, "Failed to close file.");

  file = NULL;

  return EXT_OK;
 error:
  return EXT_ERR;
}

