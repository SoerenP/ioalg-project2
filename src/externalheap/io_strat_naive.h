#ifndef __IO_STRAT_NAIVE_H
#define __IO_STRAT_NAIVE_H

#include <stdio.h>
#include <externalheap/ext_heap.h>

int io_strat_naive_read(FILE *file, int num_elements, size_t element_size, int *buffer, struct meta *m);

int io_strat_naive_write(FILE *file, int num_elements, size_t element_size, int *buffer, struct meta *m);

FILE *io_strat_naive_open(const char *filename, const char *mode, struct meta *m);

int io_strat_naive_close(FILE *file, struct meta *m);

#endif // __IO_STRAT_NAIVE_H
