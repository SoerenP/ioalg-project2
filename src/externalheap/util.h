#ifndef _UTIL_H_
#define _UTIL_H_

#include <string.h>
#include <stddef.h>
#include <externalheap/bool.h>

#define UTIL_NAME_SIZE 128
#define UTIL_STD_NODE_PATH "tests/testfiles/node_"
#define UTIL_STD_SOURCENODE_PATH "tests/testfiles/source_"
/*
 * For utility functions that dont belong to a specific module
 *
 */

void next_file_name(char *buffer, size_t buffer_size, const char *prefix, bool reset);

#endif
