#ifndef __STRATEGIES_H_
#define __STRATEGIES_H_

/* Returns int error codes
 * Params:
 * *FILE : file
 * int   : # elements to read
 * size_t: size of each element
 * *int  : buffer to read to
 */
typedef int (*ext_read) (FILE*, int, size_t, int*, struct meta*);

/* Params:
 * *FILE : file
 * int   : # elements to write
 * size_t: size of each element
 * *int  : buffer to write from
 */
typedef int (*ext_write)(FILE*, int, size_t, int*, struct meta*);

/* Returns: file pointer
 * Params:
 * *char : filename
 * *char : mode (append, truncate, write, etc.)
 */
typedef FILE *(*ext_open)(const char*, const char*, struct meta*);

/* Returns: error code
 * Params: *FILE : file pointer
 */
typedef int (*ext_close)(FILE*, struct meta*);

#endif //__STRATEGIES_H_
