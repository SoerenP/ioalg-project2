#ifndef __IO_STRAT_STD_H
#define __IO_STRAT_STD_H

#include <stdio.h>
#include <externalheap/ext_heap.h>

int io_strat_std_read(FILE *file, int num_elements, size_t element_size, int *buffer, struct meta *m);

int io_strat_std_write(FILE *file, int num_elements, size_t element_size, int *buffer, struct meta *m);

FILE *io_strat_std_open(const char *filename, const char *mode, struct meta *m);

int io_strat_std_close(FILE *file, struct meta *m);

#endif // __IO_STRAT_STD_H
