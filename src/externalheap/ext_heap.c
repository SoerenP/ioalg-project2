#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <externalheap/merge_heap.h>
#include <externalheap/ext_heap.h>
#include <externalheap/ext_node.h>
#include <externalheap/bool.h>
#include <externalheap/util.h>
#include <externalheap/meta.h>
#include <externalheap/dbg.h>
#include <externalheap/io_strat_std.h>

void printfile(const char *name, int B){
  struct meta *m = ext_meta_create(STD, B);
  FILE *file = io_strat_std_open(name, EXT_MODE_R_PLUS, m);
  int buf;
  int old = 0;
  int index = 0;
  debug("\t node %s\n",name);
  while(io_strat_std_read(file, 1, sizeof(int), &buf, m) > 0)
    {
      if (!(old <= buf)) {debug("Not sorted order internally in node.");}

      debug("\t\tread %d at index %d\n",buf,index);
      index++;
    }

  io_strat_std_close(file,m);
  ext_meta_destroy(m);
}

int count_elements_in_heap(struct ext_heap *Q){
  int i;
  int count = 0;

  for (i = 0; i < Q->size; i++){
    int tmp = Q->nodes[i]->num_elements - Q->nodes[i]->deleted;
    if  (tmp < 0){
      debug("Node %d had a negative amount of elements!!", i);
      //print_node(Q->nodes[i]);
    }
    count += tmp;
  }

  return count + Q->insert_buffer->idx;
}

// Assumes file is not opened already, and doesn't close
static FILE* open_and_skip_deleted(struct ext_heap *heap, int node_index){
  int buf, rc;
  int index = 0;
  struct ext_node *node = heap->nodes[node_index];

  FILE *file = (*heap->open)(node->filename, EXT_MODE_R_PLUS, node->m);


  while (index < node->deleted){
    rc = (*heap->read)(file, 1, node->element_size, &buf, node->m);
    check(rc != EXT_ERR || buf >= 0, " Read nothing, but expected to skip something.");
    index++;
  }

  return file;

 error:
  rc = (*heap->close)(file,node->m);
  check(rc != EXT_ERR, "Failed to close file.");
  return NULL;
}

struct ext_heap *ext_heap_create(int max_size, int M, int B, enum strategy strat, ext_write w, ext_open o, ext_read r, ext_close c)
{
  if (strat == MMAP)
    check(B%4096 == 0, "Wrong Block size for MMAP strategy. Got %d but wanted some B mod 4096==0", (B%4096));

  struct ext_heap *heap = malloc(sizeof(*heap));
  check_mem(heap);

  /* allocate the buffers */
  heap->insert_buffer = merge_heap_create(M);
  check_mem(heap->insert_buffer);

  heap->merging_buffer = merge_heap_create(M+1);
  check_mem(heap->merging_buffer);

  heap->root_min_elements = merge_heap_create(B);
  check_mem(heap->root_min_elements);

  /* allocate the array for the ext_nodes */
  heap->nodes = malloc(sizeof(struct ext_node *)*max_size);
  check_mem(heap->nodes);
  int index;
  for(index = 0; index < max_size; index++)
    {
      heap->nodes[index] = NULL;
    }

  /* parameters */
  heap->M = M;
  heap->B = B;
  heap->size = 0;
  heap->max_size = max_size;
  heap->threshold = (int)ceil(M/2);

  /* function pointers */
  heap->strat = strat;
  heap->write = w;
  heap->open = o;
  heap->read = r;
  heap->close = c;

  return heap;
 error:
  return NULL;
}

void ext_heap_destroy(struct ext_heap *heap)
{
  if(heap){
    if(heap->insert_buffer)
      merge_heap_destroy(heap->insert_buffer);

    if(heap->merging_buffer)
      merge_heap_destroy(heap->merging_buffer);

    if(heap->root_min_elements)
      merge_heap_destroy(heap->root_min_elements);

    if(heap->nodes){
      int index;
      for(index = 0; index < heap->size; index++) //max->size eller size?
        {
          if(heap->nodes[index]){
            ext_node_destroy(heap->nodes[index]);
            heap->nodes[index] = NULL;
          }
        }
      free(heap->nodes);
      heap->nodes = NULL;
    }

    free(heap);
  }
}

/* c'th child can be found at
 * fanout * index + 1 + c
 * parent can be found at
 * floor( (index - 1) / fanout )
 */
int heap_child(int parent_index, int fanout, int c)
{
  return fanout * parent_index + 1 + c;
}

int heap_parent(int child_index, int fanout)
{
  return (int)floor((float)(child_index - 1) / (float)fanout);
}

static void merge_leaves(struct ext_heap *heap, int child_idx, int parent_idx){
  // open parent and insert all of it into heap
  ext_node_read_heap(heap->merging_buffer,
                     heap->nodes[parent_idx],
                     parent_idx,
                     heap->nodes[parent_idx]->deleted,
                     heap->open, heap->read, heap->close);
  // open child and insert one element at a time into heap, continuesly writing to the
  // parent file
  ext_node_read_and_write_heap(heap->merging_buffer,
                               heap->nodes[child_idx],
                               heap->nodes[parent_idx],
                               heap->M,
                               heap->open, heap->read, heap->write, heap->close);
  if (parent_idx == 0){
    // clear the root_min buffer
    merge_heap_destroy(heap->root_min_elements);
    heap->root_min_elements = merge_heap_create(heap->B);

    // update the root buffer with the newly inserted elements
    ext_node_read_into_rootblock(heap->root_min_elements,
                                 heap->nodes[parent_idx],
                                 heap->B,
                                 0,
                                 heap->open, heap->read, heap->close);
  }
}

static void sift_up(struct ext_heap *heap, int index){
  int indexOfParent = heap_parent(index, heap->M/heap->B);
  int inserted = index;
  while (0 < inserted &&
         heap->nodes[inserted]->min < heap->nodes[indexOfParent]->max) {
    merge_leaves(heap, inserted, indexOfParent);
    inserted = indexOfParent;
    indexOfParent = heap_parent(inserted, heap->M/heap->B);
  }
}

static void swap(struct ext_heap *heap, int idx1, int idx2){
  struct ext_node *tmp = heap->nodes[idx1];
  heap->nodes[idx1] = heap->nodes[idx2];
  heap->nodes[idx2] = tmp;
}

static void insert_buffer_into_tree(struct ext_heap *heap)
{
  /* if the heap is not empty, insert as normal */
  if(heap->size != 0 ){
    // create new node "last leaf"
    char name[UTIL_NAME_SIZE];
    next_file_name(name, UTIL_NAME_SIZE, UTIL_STD_NODE_PATH, FALSE);

    check(heap->size + 1 <= heap->max_size, "Heap is full. Oh no.");

    struct ext_node *old_last_leaf = heap->nodes[heap->size - 1];

    heap->nodes[heap->size] = ext_node_create(name, heap->M,sizeof(int),heap->strat,heap->B);
    struct ext_node *new_node = heap->nodes[heap->size];
    heap->size++;

    ext_node_write_heap(heap->insert_buffer, new_node, heap->open, heap->write, heap->close);

    // is the old last leaf satisfied by load condition? if no, swap and sift up both
    int old_last_leaf_elements = old_last_leaf->num_elements - old_last_leaf->deleted;
    if (old_last_leaf_elements < heap->M/2 ||
        old_last_leaf_elements > heap->M){
      // swap pointers to the two last leaves in parents, and
      // sift both up
      swap(heap, heap->size-2, heap->size-1);
      sift_up(heap, heap->size-2);
      sift_up(heap, heap->size-1);
    } else {
      //no reason to swap, so just sift up the new leaf only.
      sift_up(heap,heap->size - 1);
    }
  } else { //the heap is empty, insert the last_leaf / root
    char name[UTIL_NAME_SIZE];
    next_file_name(name, UTIL_NAME_SIZE, UTIL_STD_NODE_PATH, TRUE);

    heap->nodes[heap->size] = ext_node_create(name, heap->M, sizeof(int), heap->strat, heap->B);
    struct ext_node *new_node = heap->nodes[heap->size];

    heap->size++;
    ext_node_write_heap(heap->insert_buffer, new_node, heap->open, heap->write, heap->close);

  }

 error:
  return;
}

int ext_heap_insert(struct ext_heap *heap, int i){
  /* first insert into insert buffer */
  merge_heap_insert(heap->insert_buffer, item_create(i,0));
  /* check if i contains m elements */
  if(heap->insert_buffer->idx == heap->M)
    {
      insert_buffer_into_tree(heap);
    }
  return EXT_OK;
}

int ext_heap_read_node_into_buffer(struct ext_heap *heap, int node_index, struct merge_heap *merge_heap)
{
  struct ext_node *node = heap->nodes[node_index];
  FILE *file = open_and_skip_deleted(heap, node_index);
  check(file != NULL, "Could either not skip in file, or open.");

  /* read acutal elements */
  int buf, rc, read = 0;
  int node_elements = node->num_elements - node->deleted;

  while(read < node_elements)
    {
      rc = (*heap->read)(file,1,node->element_size,&buf,node->m);
      if(rc > 0){
        read++;
        merge_heap_insert(merge_heap, item_create(buf,node_index));
      } else {
        break;
      }
    }

  rc = (*heap->close)(file,node->m);

  check(rc != EXT_ERR, "Failed to close file.");
  check(read == node_elements, "Failed to read entire node into merge heap, read %d elements, wanted to read %d elements, node index: %d", read, node_elements,node_index);

  return EXT_OK;
 error:
  return EXT_ERR;
}


/* can at max have fanout children.*/
/* cant at min have 0 children. This is the case if your 0'th child should be at an index */
/* that surpasses (heapsize -1) - then you are a leaf! */
static int number_of_children(struct ext_heap *heap, int parent_index)
{
  int heapsize = heap->size;
  int fanout = heap->M/heap->B;
  int i = 0;
  while((heap_child(parent_index,fanout,i) < heapsize) && (i <= fanout)) {i++;}
  return i;
}

static int get_total_elements_in_children(struct ext_heap *heap, int parent_index)
{
  int children = number_of_children(heap,parent_index);
  int fanout = heap->M/heap->B;
  int current_child = 0;
  int total_elements = 0;
  for(current_child = 0; current_child < children; current_child++)
    {
      struct ext_node *child = heap->nodes[heap_child(parent_index,fanout,current_child)];
      total_elements = total_elements + (child->num_elements - child->deleted);
    }
  return total_elements;
}

/*
 * It has been asserted when calling this that the children have enough elements to give
 * to the parent, as to make the parent valid again.
 * We insert all parents elements into merge buffer, insert one element from each child,
 * if we extract an element from one of the children, we increment its deleted counter,
 * and insert another element from that child into merging.
 * Afterwards, the parent has its counters reset to zero, since it is purged (so delete = 0)
 * Merge buffer is reset when we are done, but assumed clean on when starting!
 */

int ext_heap_merge_node_with_children(struct ext_heap *heap, int parent_index)
{
  /* how many elements do we need? */
  struct ext_node *parent = heap->nodes[parent_index];

  int is_naive = 0;
  if(parent->m->s == NAIVE) is_naive = 1;

  int threshold = heap->threshold;
  int fanout = heap->M/heap->B;

  int elements_needed = threshold;
  int parent_elements_before = parent->num_elements - parent->deleted;

  /* how many children do we have? */
  int children = number_of_children(heap, parent_index);

  /* open children, and take first element of each into merge buffer */
  /* but SKIP deleted elements!!! */
  int curr_child, buf, rc;
  FILE *children_files[children];

  //what if one of children has 0 elements here, we still insert one and call delete++?
  for(curr_child = 0; curr_child < children; curr_child++)
    {
      int child_index = heap_child(parent_index,fanout,curr_child);
      struct ext_node *child_node = heap->nodes[child_index];

      int elements_in_child = child_node->num_elements - child_node->deleted;
      if(elements_in_child <= 0)
        {
          debug("NO ELEMENTS(%d) IN CHILD %d WHEN READING FIRST INTO MERGE HEAP?\n",elements_in_child,child_index);
        }

      /* Skip deleted */
      children_files[curr_child] = open_and_skip_deleted(heap, child_index);
      check(children_files[curr_child], "Could not open file, or could not skip all deleted");

      /* we skipped deleted, now read actual element, and give it list = its inded in heap */
      rc = (*heap->read)(children_files[curr_child], 1, child_node->element_size, &buf, child_node->m);
      check(rc != EXT_ERR, "Failed to read element from child when refilling parent.");
      if(rc == 0){ debug("REACHED EOF WHILE TRYING TO INSERT FIRST INTO MERGE BUFFER?");}
      merge_heap_insert(heap->merging_buffer, item_create(buf,child_index));
    }

  /* open parent file, take ALL Elements to merging heap, with parent index as item.list */
  ext_heap_read_node_into_buffer(heap, parent_index, heap->merging_buffer);

  /* open again, but purge it, and reset its deleted counter */
  FILE *parent_file = (*heap->open)(parent->filename,EXT_MODE_W_PLUS,parent->m);
  parent->deleted = 0;
  parent->num_elements = 0;
  parent->min = -1;
  parent->max = -1;

  /* steal elements from children until parent has enough elements. Merge buffer already has all elements from parent */
  buf = -1; // debugging
  int end = 0;
  int read = 0;
  while(read < elements_needed + parent_elements_before)
    {
      /* take the minimal element out of the merge heap */
      struct item temp = merge_heap_delete_min(heap->merging_buffer);
      if(temp.key == MERGE_HEAP_ERR)
        {
          sentinel("Mergeheap empty when stealing from children");
          break;
        }

      /* write to parent, update min and max */
      (*heap->write)(parent_file,1,parent->element_size,&temp.key,parent->m);
      heap->nodes[parent_index]->num_elements++;
      if (read == 0)
        {
          heap->nodes[parent_index]->min = temp.key;
        }
      heap->nodes[parent_index]->max = temp.key;
      read++;

      /* if we read from the parent itself, we should not read anything new in */
      /* if we read from something != parent, increment its deleted and read its next into the heap, unless it was or became empty */
      if(temp.list != parent_index){
        struct ext_node *child = heap->nodes[temp.list];
        /* is child already empty ? */
        if((child->num_elements - child->deleted) != 0)
          {
            child->deleted++;
            /* now child might have become empty!! */
            if (child->num_elements - child->deleted <= 0)
              {
                /* noting to do, skip to next round */
              }
            else
              {
                /* is this needed now? */
                //if((child->m->s == NAIVE) && (read == elements_needed + parent_elements_before)) break;

                /* child is not empty. Read next element from that child */
                int nr_child = temp.list - (fanout * parent_index + 1);
                int rc = (*heap->read)(children_files[nr_child],1,child->element_size,&buf,child->m);
                if(rc != EXT_ERR) //skal vi også teste for eof?
                  {
                    struct item new = item_create(buf,temp.list);
                    merge_heap_insert(heap->merging_buffer,new);
                    heap->nodes[temp.list]->min = buf;
                  }
                else
                  {
                    /* child "not" empty, but we failed to read. Mark some child as dead with end, and check if everyone is dead */
                    /* should never happen, unless your IO methods suddenly crash and burn */
                    end++;
                    if(end == children)
                      {
                        sentinel("All children exhausted when refilling parent, should never happen!!!");
                        break;
                      }
                  }
              }
          }
        else
          {
            debug("exhausted child %d, num elements in heap %d, end %d\n",temp.list,count_elements_in_heap(heap),end);
          }
      }

      /* check if we are done */
      if(read == elements_needed + parent_elements_before){
        break;
      }
    }
  check(read == elements_needed + parent_elements_before, "Did not read enough elements");

  /* if naive, and elements in mergeheap, extract them and fix the index of the child and the deleted counter */
  while((heap->merging_buffer->idx > 0) && (is_naive))
    {
      struct item dirtyhack = merge_heap_delete_min(heap->merging_buffer);
      if(dirtyhack.list != parent_index){
        struct ext_node *child = heap->nodes[dirtyhack.list];
        if(child->m->s == NAIVE)
          {
            /* decrement its index. if it hits < 0, set it to bloksize -1 ? */
            child->m->mn->index--;
          }
      }
    }


  // clean up the mergebuffer here instead.
  merge_heap_destroy(heap->merging_buffer);
  heap->merging_buffer = merge_heap_create(heap->M+1);

  //print_node(heap->nodes[parent_index]);
  /* close everything */
  (*heap->close)(parent_file,parent->m);

  int child = 0;
  for(child = 0; child < children; child++)
    {
      struct ext_node *child_node = heap->nodes[heap_child(parent_index,fanout,child)];
      (*heap->close)(children_files[child],child_node->m);
    }

  return EXT_OK;
 error:
  return EXT_ERR;
}

int recursively_fill_parent(struct ext_heap *heap, int parent_index)
{
  /* how many elements to we need */
  int threshold = heap->threshold;
  int fanout = heap->M/heap->B;
  int elements_needed = threshold;

  /* do our children have enough? */
  int elements_in_children = get_total_elements_in_children(heap, parent_index);
  int children = number_of_children(heap, parent_index);

  if(elements_in_children >= elements_needed)
    {
      /* if yes, take what you need */
      ext_heap_merge_node_with_children(heap, parent_index);

      /* the above call might have changed number of children? */
      int children_after = number_of_children(heap, parent_index);
      /* now children might not be perfect, recurse on them */
      int child = 0;
      for(child = 0; child < children_after; child++)
        {
          // Does the child exist in the tree?
          if ( heap_child(parent_index, fanout, child) >= heap->size ){
            break;
          }

          struct ext_node *child_node = heap->nodes[heap_child(parent_index,fanout,child)];
          if (child_node != NULL){
            if((child_node->num_elements - child_node->deleted) < threshold){
              recursively_fill_parent(heap, heap_child(parent_index,fanout,child));
            }
          } else {
            debug("\tchild at index: %d was null. \n", child);
          }
        }
    }
  else if((children == 1) && (elements_in_children < elements_needed))
    {
      /* if only one child, take all its elements and delete it */
      int fanout = heap->M/heap->B;
      int child_index = heap_child(parent_index,fanout,0);
      merge_leaves(heap, child_index, parent_index);
      ext_node_destroy(heap->nodes[child_index]);
      heap->nodes[child_index] = NULL;
      heap->size--;
      // note: this might cause us to be a leaf, and will be handled below
    }

  /* if we now became a leaf, or if we were all along, we might be imperfect. If so,steal from last leaf if possible */
  /* if we have no children, we are a leaf */
  int children_after = number_of_children(heap, parent_index);
  if(children_after <= 0)
    {
      /* if we are last leaf, do nothing */
      int last_leaf_index = heap->size-1;
      if(parent_index == last_leaf_index)
        {
          if (heap->nodes[last_leaf_index]->num_elements - heap->nodes[last_leaf_index]->deleted <= 0)
            {
              ext_node_destroy(heap->nodes[last_leaf_index]);
              heap->nodes[last_leaf_index] = NULL;
              heap->size--;
            }
          return EXT_OK;
        }
      else
        /*otherwise, steal records from the last leaf */
        {
          /* 3 posibilites, depending on s = [our elements] + [last leaf elements] */
          struct ext_node *current_node = heap->nodes[parent_index];
          struct ext_node *last_leaf = heap->nodes[last_leaf_index];

          int current_node_elements  = current_node->num_elements - current_node->deleted;
          int last_leaf_elements = last_leaf->num_elements - last_leaf->deleted;
          int sum_elements = current_node_elements + last_leaf_elements;

          int threshold = heap->threshold;
          if(sum_elements > heap->M)
            {
              /* take heap->M - current_node_elements from last leaf to current */
              merge_leaves(heap, last_leaf_index, parent_index);
              /* now sift up current_node */
              sift_up(heap, parent_index);
            }
          else if((sum_elements >= threshold) && (heap->M >= sum_elements))
            {
              /* merge current_node and last leaf, s.t current_node gets everything and last leaf deleted. */
              merge_leaves(heap, last_leaf_index, parent_index);
              check(last_leaf->num_elements - last_leaf->deleted == 0, "Last leaf should be empty");
              ext_node_destroy(heap->nodes[last_leaf_index]);
              heap->nodes[last_leaf_index] = NULL;
              heap->size--;

              /* sift up current_node */
              sift_up(heap, parent_index);
            }
          else if(sum_elements < threshold)
            {
              /* take everything from last leaf, find new last leaf, take enough from it and sit up */
              int last_leaf_elements = heap->nodes[last_leaf_index]->num_elements - heap->nodes[last_leaf_index]->deleted;
              if(last_leaf_elements != 0){
                merge_leaves(heap, last_leaf_index, parent_index);
                check(last_leaf->num_elements - last_leaf->deleted == 0, "Last leaf should be empty");
              }
              ext_node_destroy(heap->nodes[last_leaf_index]);
              heap->nodes[last_leaf_index] = NULL;
              heap->size--;

              int new_last_leaf_index = heap->size-1;
              int current_index = parent_index;
              if(current_index == new_last_leaf_index)
                {
                  /* we are the new last leaf. sift it up. */
                  sift_up(heap,current_index);
                }
              else /* due to load conditions, we are guaruanteed that new_last_leaf has >= threshold */
                {
                  /* fill up the current leaf until it has enough, sift it up, leaving last leaf with something, doesnt matter how much */
                  /* we know that new_last_leaf was an internal node before, so it has above threshold elements. */
                  /* we also know that we have sum_elements < threshold, so if we steal threshold we have */
                  /* < m elements but > threshold, so we are okay, so sift up */
                  struct ext_node *new_last_leaf = heap->nodes[new_last_leaf_index];
                  merge_leaves(heap, new_last_leaf_index, current_index);

                  /* new last leaf might be empty, destroy it */
                  if((new_last_leaf->num_elements - new_last_leaf->deleted) <= 0){
                    ext_node_destroy(heap->nodes[new_last_leaf_index]);
                    heap->nodes[last_leaf_index] = NULL;
                    heap->size--;
                  }

                  sift_up(heap,current_index);
                }
            }
        }
    }

  return EXT_OK;
 error:
  return EXT_ERR;
}

int ext_heap_delete_min(struct ext_heap *heap){
  /* extract min from insert buffer and from root_list_min_buffer. Return the minimal, insert the other again */
  struct item min_insert_buffer = merge_heap_delete_min(heap->insert_buffer);
  struct item min_root_min_elements = merge_heap_delete_min(heap->root_min_elements);

  struct ext_node *root = heap->nodes[0];

  /* check if root_min_elements was empty, try to read the next elements. */
  if(min_root_min_elements.key == MERGE_HEAP_ERR)
    {
      if((root == NULL) && (min_insert_buffer.key != MERGE_HEAP_ERR))
        {
          return min_insert_buffer.key;
        }
      int rc = ext_node_read_into_rootblock
        (heap->root_min_elements,root,heap->B,root->deleted,heap->open,heap->read,heap->close);
      check(rc != EXT_ERR, "Failed to read block of root into memory");
      min_root_min_elements = merge_heap_delete_min(heap->root_min_elements);
    }

  int min_element = MERGE_HEAP_ERR;

  /* one might be empty. If both empty, return error */
  if(min_insert_buffer.key == MERGE_HEAP_ERR)
    {
      if(min_root_min_elements.key == MERGE_HEAP_ERR){
        debug("both empty?\n");
        return MERGE_HEAP_ERR; //both empty
      }
      else {
        min_element = min_root_min_elements.key; //rootlist wasnt empty, so its the smaller one
        root->deleted++;
      }
    } else if(min_root_min_elements.key == MERGE_HEAP_ERR){
    min_element = min_insert_buffer.key; //insert buffer cant be empty, would be caugt above, return its min
  } else { //none of them are empty, find the min, insert the other into its heap again.
    if(min_root_min_elements.key < min_insert_buffer.key){
      min_element = min_root_min_elements.key;
      root->deleted++;
      merge_heap_insert(heap->insert_buffer,min_insert_buffer);
    } else {
      min_element = min_insert_buffer.key;
      merge_heap_insert(heap->root_min_elements,min_root_min_elements);
    }
  }

  /* now min_element should be set, or we returned already*/
  /* if the root_min_elements is empty, we need to read in the next B elements from the root ext node */
  if(min_root_min_elements.key == MERGE_HEAP_ERR){
    int rc = ext_node_read_into_rootblock
      (heap->root_min_elements,root,heap->B,root->deleted,heap->open,heap->read,heap->close);

    check(rc != EXT_ERR, "Failed to read next elements into rootblock during delete min");
  }

  /* root may be not be perfect, i.e node->num_elements - heap->deleted < m/2 */
  int root_elements = root->num_elements - root->deleted;
  int threshold = heap->threshold;
  int fanout = heap->M/heap->B;
  if(root_elements <= threshold) //<= or <?
    {
      int root_index = 0;
      int elements_needed = threshold;
      int elements_in_children = get_total_elements_in_children(heap,root_index);
      int children = number_of_children(heap, root_index);
      if(elements_in_children >= elements_needed)
        {
          debug("children do have enough\n");
          /* if children have enough elements, steal from them */
          ext_heap_merge_node_with_children(heap, root_index);

          /* this means we should update the root min elements buffer since root was changed */
          /* first destroy it, then create and read into it */
          merge_heap_destroy(heap->root_min_elements);
          heap->root_min_elements = merge_heap_create(heap->B);
          int rc = ext_node_read_into_rootblock
            (heap->root_min_elements, root, heap->B, root->deleted, heap->open, heap->read, heap->close);

          check(rc != EXT_ERR, "Failed to read a block of root node into internal memory.");

          /* the above merging might have killed of children, update */
          int children_after = number_of_children(heap, root_index);

          /* now children might not be perfect, recurse on them */
          int child = 0;
          for(child = 0; child < children_after; child++)
            {
              // Does the child exist in the tree?
              if ( heap_child(root_index, fanout, child) >= heap->size ){
                break;
              }

              struct ext_node *child_node = heap->nodes[heap_child(root_index,fanout,child)];
              if (child_node != NULL){
                int child_elements = child_node->num_elements - child_node->deleted;
                if(child_elements < threshold){
                  recursively_fill_parent(heap, heap_child(root_index, fanout, child));
                }
              } else {
                debug("\tbottom : child at index: %d was null. \n", child);
              }
            }
        }

      /* if only a single child with less than enough elements, take all its elements and delete it */
      else if((children == 1) && (elements_in_children < elements_needed))
        {
          debug("only one child, eat it as root\n");
          int fanout = heap->M/heap->B;
          int child_index = heap_child(root_index,fanout,0);
          /* merge leaves doesnt take all elements, only m */
          ext_node_read_and_write_heap(
                                       heap->merging_buffer,
                                       heap->nodes[child_index],
                                       heap->nodes[root_index],
                                       elements_in_children,
                                       heap->open,heap->read,heap->write,heap->close);
          ext_node_destroy(heap->nodes[child_index]);
          heap->nodes[child_index] = NULL;
          heap->size--;

          /* update internal buffer of block of root to be up to date */
          int rc = ext_node_read_into_rootblock
            (heap->root_min_elements,root,heap->B,root->deleted,heap->open,heap->read,heap->close);
          check(rc != EXT_ERR, "Failed to read a block of root node into internal memory.");
        }
    }

  return min_element;
 error:
  return EXT_ERR;
}

int ext_heap_sort
(struct ext_node *input_file, struct ext_node *output_file, int N, int M, int B,
 enum strategy strat, ext_write w, ext_open o, ext_read r, ext_close c )
{

  /* create the heap */
  int heap_max_size = ((int)ceil((double)N/(double)M)) + 1;
  struct ext_heap *heap = ext_heap_create(heap_max_size,M,B,strat,w,o,r,c);

  /* insert all elements from the input file */
  FILE *file = (*o)(input_file->filename,EXT_MODE_R_PLUS,input_file->m);
  int index = 0;
  int rc;
  int buf = 0;
  for(index = 0; index < N; index++)
    {
      rc = (*r)(file,1,input_file->element_size,&buf,input_file->m);
      check(rc != EXT_ERR, "Failed to read from inputfile");
      ext_heap_insert(heap,buf);
    }
  (*c)(file,input_file->m);

  /* continiuosly delete the minimal element and put it in the output_file */
  FILE *output = (*o)(output_file->filename,EXT_MODE_W_PLUS,output_file->m);
  int deleted_min = 0;
  for(index = 0; index < N; index++)
    {
      deleted_min = ext_heap_delete_min(heap);
      rc = (*w)(output,1,sizeof(int),&deleted_min,output_file->m);
      check(rc != EXT_ERR, "Failed to write to output");
    }
  (*c)(output,output_file->m);

  /* cleanup */
  ext_heap_destroy(heap);

  return EXT_OK;
 error:
  return EXT_ERR;
}
