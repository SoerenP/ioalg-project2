#!/bin/bash

# Make plots    
for i in experiments/*.gp; do
  echo "MAKING $i"
  gnuplot $i
done
