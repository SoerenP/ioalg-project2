#!/bin/bash

mkdir -p experiments/pngs
mkdir -p experiments/results

#rm experiments/results/* -f
#rm experiments/pngs/* -f

maxN=22
maxM=20 ## implicitly also maxB
maxB=20

strategies=`seq 0 2`

# # Lock N, Vary M and B, for each strategy.
echo "Lock N, Vary M and B, for each strategy."
for s in $strategies;
do
    N=`dc -e "2 $maxN ^ p"`
    ## Vary M (from pagesize(4096 bytes) and upwards)
    for m in `seq 14 $maxM`;
    do
        M=`dc -e "2 $m ^ p"`
        ## Vary B according to M (we can't get larger than M)
        for b in `seq 14 $m`;
        do
            B=`dc -e "2 $b ^ p"`
            echo "S: $s, N: $N, M: $M, B: $B"
            experiments/main $N $M $B $s >> experiments/results/lock-n-vary-m-b-s-$s.gdata
        done
    done
done    

# Lock N, Lock M, vary B, for each strategy.
echo "Lock N, Lock M, vary B, for each strategy."
for s in $strategies;
do
    N=`dc -e "2 $maxN ^ p"`
    M=`dc -e "2 $maxM ^ p"`
    ## Vary B according to M (we can't get larger than M)
    for b in `seq 14 $maxM`;
    do
        B=`dc -e "2 $b ^ p"`
        echo "S: $s, N: $N, M: $M, B: $B"
        experiments/main $N $M $B $s >> experiments/results/lock-n-m-vary-b-s-$s.gdata
    done
done    

# Lock N, Lock B, Vary M
echo "Lock N, Lock B, vary M, for each strategy."
for s in $strategies;
do
    N=`dc -e "2 $maxN ^ p"`
    B=`dc -e "2 14 ^ p"`
    ## Vary M (from the size of the B (4096 bytes) and upwards)
    for m in `seq 14 $maxM`;
    do
        M=`dc -e "2 $m ^ p"`
        echo "S: $s, N: $N, M: $M, B: $B"
        experiments/main $N $M $B $s >> experiments/results/lock-n-b-vary-m-s-$s.gdata
    done
done    

# Lock M, Lock B, Vary N
echo "Lock M, Lock B, vary N, for each strategy."
for s in $strategies;
do
    for n in `seq 8 28`;
    do
        N=`dc -e "2 $n ^ p"`
        B=`dc -e "2 18 ^ p"`
        M=`dc -e "2 19 ^ p"`
        echo "S: $s, N: $N, M: $M, B: $B"
        START=$(date +%s.%N)
        experiments/main $N $M $B $s >> experiments/results/lock-m-b-vary-n-s-$s.gdata
        END=$(date +%s.%N)
        DIFF=$(echo "$END - $START" | bc)
        echo "Time elapsed: $DIFF"
    done
done    

# Run the old experiments, where we figured M=B was the best configuration.
echo "Varying N, other sorting algos"
for n in `seq 8 28`;
do
    d=2
    N=`dc -e "2 $n ^p "`
    M=`expr $N / 2`
    echo "N: $N, M: $M, B: $B, d: $d"
    START=$(date +%s.%N)
    experiments/experiment_heapsort $N >> experiments/results/heapsort.gdata
    END=$(date +%s.%N)
    DIFF=$(echo "$END - $START" | bc)
    echo "Time elapsed: $DIFF"
    rm tests/testfiles/* -f 
    
    START=$(date +%s.%N)
    experiments/experiment_quicksort $N >> experiments/results/quicksort.gdata
    END=$(date +%s.%N)
    DIFF=$(echo "$END - $START" | bc)
    echo "Time elapsed: $DIFF"
    rm tests/testfiles/* -f
    
    START=$(date +%s.%N)
    experiments/experiment_mergesort $N $M $d "someprefix" >> experiments/results/mergesort.gdata
    END=$(date +%s.%N)
    DIFF=$(echo "$END - $START" | bc)
    echo "Time elapsed: $DIFF"
    rm tests/testfiles/* -f
done

# Make plots    
for i in experiments/*.gp; do
  echo "MAKING $i"
  gnuplot $i
done
