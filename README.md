#####Contributors: Søren Elmely Pettersson & Troels Thorsen


####BUILDING EVERYTHING
Make clean all will build all the sources into two statically linked libraries, and run all tests of the current project automatically. 

linking static libraries is OS specific. I.e on linux distributions, the linking is after the source names, as the makefile ensures.
On MAC IOS however, the linking must before. As such, the linking with the current makefile will most likely fail on MAC. Try manipulating the 
order of linking in the makefile if this issue arrises. 

Platforms the makefile is confirmed to build on, are the following:

Linux Ubuntu 14.04


####Bugs

No known bugs. The implementation assumes that all parameters are powers of two. We do not check exlicitly for this,
so deviating from this migth introduce undefined behavioour.

Everything was run under valgrind for appropriate parameters - the implementations, experiments and tests report no leakage,
or any errors/invalid reads or writes / memory corruption to our knowledge. 

The implementation will attempt to create files when run, so appropriate permissions must be given to the running process. Use at your own precaussion. 


