#!/bin/bash

mkdir -p experiments/pngs
mkdir -p experiments/results

maxN=24
maxM=24 
maxB=17

strategies=`seq 0 2`

# Lock N, Lock B, Vary M
echo "Lock N, Lock B, vary M, for each strategy."
for s in $strategies;
do
    N=`dc -e "2 $maxN ^ p"`
    B=`dc -e "2 $maxB ^ p"`
    ## Vary M (from the size of the B (4096 bytes) and upwards)
    for m in `seq $maxB $maxM`;
    do
        M=`dc -e "2 $m ^ p"`
        echo "S: $s, N: $N, M: $M, B: $B"
        START=$(date +%s.%N)
        experiments/main $N $M $B $s >> experiments/results/lock-n-b-vary-m-s-$s.gdata
        END=$(date +%s.%N)
        DIFF=$(echo "$END - $START" | bc)
        echo "Time elapsed: $DIFF"
    done
done    
