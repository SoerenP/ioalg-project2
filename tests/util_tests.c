#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <externalheap/minunit.h>
#include <externalheap/util.h>

#define TEST_SIZE 32

static char *prefix = "test";
static char *result_0 = "test0";
static char *result_1 = "test1";
static char *result_2 = "test2";

static char buffer[TEST_SIZE] = {0};

char *test_init()
{

  next_file_name(buffer,TEST_SIZE,prefix,FALSE);
  mu_assert(strcmp(buffer,result_0) == 0, "First name should be appended with 0");

  next_file_name(buffer,TEST_SIZE,prefix,FALSE);
  mu_assert(strcmp(buffer,result_1) == 0, "Second name should be appended with 1");

  next_file_name(buffer,TEST_SIZE,prefix,FALSE);
  mu_assert(strcmp(buffer,result_2) == 0, "Third name should be appended with 3");

  next_file_name(buffer,TEST_SIZE,prefix,TRUE);
  mu_assert(strcmp(buffer,result_0) == 0, "After reset appended should be 0");

  return NULL;
}

char *all_tests()
{
  mu_suite_start();

  mu_run_test(test_init);


  return NULL;
}

RUN_TESTS(all_tests);
