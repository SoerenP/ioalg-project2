#include <stdio.h>
#include <time.h>

#include <externalheap/minunit.h>
#include <externalheap/ext_heap.h>
#include <externalheap/io_strat_std.h>

#define TEST_SIZE 1000
#define ELEM_SIZE sizeof(int)
#define BLOCK_SIZE 100

static int test_array[TEST_SIZE] = {0};
static const char *filename = "tests/testfiles/std_test.tmp";
static FILE *fp = NULL;
static struct meta_std inner_large_block = {.blocksize = BLOCK_SIZE};
static struct meta dummy_meta = {.s = STD, .ms = &inner_large_block};

void setup_test_array()
{
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    test_array[index] = rand();

}

char *test_open_read()
{
  fp = io_strat_std_open(filename,EXT_MODE_R,&dummy_meta);
  mu_assert(fp != NULL, "Failed to open for reading naive.");

  return NULL;
}

char *test_open_write()
{
  fp = io_strat_std_open(filename,EXT_MODE_W,&dummy_meta);
  mu_assert(fp != NULL, "Failed to open for writing naive.");

  return NULL;
}

char *test_close()
{
  int rc = io_strat_std_close(fp,&dummy_meta);
  mu_assert(rc != EXT_ERR, "Failed to close naive.");

  return NULL;
}

char *test_write_all()
{
  int rc = io_strat_std_write(fp,TEST_SIZE,ELEM_SIZE,test_array,&dummy_meta);
  mu_assert(rc != EXT_ERR, "Failed to write naive.");

  return NULL;
}

char *test_read_all()
{
  int res_array[TEST_SIZE] = {0};
  int rc = io_strat_std_read(fp,TEST_SIZE,ELEM_SIZE,res_array,&dummy_meta);
  mu_assert(rc != EXT_ERR, "Failed to read naive.");

  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      mu_assert(res_array[index] == test_array[index], "Failed to read what was written naive.");
    }

  /* try to read again, should give EOF */
  rc = io_strat_std_read(fp,TEST_SIZE,ELEM_SIZE,res_array,&dummy_meta);
  mu_assert(rc == EXT_EOF, "Should be at EOF.");

  return NULL;
}

char *test_write_singles()
{
  int index;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int rc = io_strat_std_write(fp,1,ELEM_SIZE,&test_array[index],&dummy_meta);
      mu_assert(rc != EXT_ERR, "Failed to write single element naive.");
    }
  return NULL;
}

char *test_read_singles()
{
  int index;
  int buf;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int rc = io_strat_std_read(fp,1,ELEM_SIZE,&buf,&dummy_meta);
      mu_assert(rc != EXT_ERR, "Failed to read single element.");
      mu_assert(buf == test_array[index], "Failed to read what was written");
    }

  /* try to read again, should be at EOF */
  int rc = io_strat_std_read(fp,1,ELEM_SIZE,&buf,&dummy_meta);
  mu_assert(rc == EXT_OK, "Should be at EOF.");

  return NULL;
}

char *all_tests()
{
  srand(time(NULL));
  setup_test_array();

  mu_suite_start();

  /* entire arrays at once */
  mu_run_test(test_open_write);
  mu_run_test(test_write_all);
  mu_run_test(test_close);
  mu_run_test(test_open_read);
  mu_run_test(test_read_all);
  mu_run_test(test_close);

  /* reset to be sure */
  setup_test_array();

  /* single elements at a time */
  mu_run_test(test_open_write);
  mu_run_test(test_write_singles);
  mu_run_test(test_close);
  mu_run_test(test_open_read);
  mu_run_test(test_read_singles);
  mu_run_test(test_close);

  return NULL;
}


RUN_TESTS(all_tests);
