#include <externalheap/merge_heap.h>
#include <externalheap/minunit.h>
#include <externalheap/dbg.h>
#include <externalheap/bool.h>
#include <time.h>
#include <stdlib.h>

#define TEST_SIZE 256
#define BLOCK_SIZE sizeof(int)

static struct merge_heap *h = NULL;
static struct item test_array[TEST_SIZE];

void test_array_setup()
{
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      test_array[index] = item_create(rand(), 0);
    }
}

char *test_destroy(){
  merge_heap_destroy(h);
  return NULL;
}

char *makeheap()
{
  h = merge_heap_create(TEST_SIZE);
  mu_assert(NULL != h, "Failed to make heap, fuck..");

  return NULL;
}

char *insertion(){
  int index;
  for (index = 0; index < TEST_SIZE; index++){
    mu_assert(MERGE_HEAP_OK == merge_heap_insert(h, test_array[index]), "could not insert into heap");
  }

  return NULL;
}

char *delmin(){
  int index;
  for (index = 0; index < TEST_SIZE; index++){
    test_array[index] = merge_heap_delete_min(h);
  }

  for (index = 0; index < TEST_SIZE-1; index++){
    mu_assert(test_array[index].key <= test_array[index+1].key, "array was not sorted");
  }
  return NULL;
}

char *test_heapsort_inplace()
{
  test_array_setup();

  merge_heap_heapsort_inplace(test_array, TEST_SIZE);

  int index = 0;
  for(index = 0; index < TEST_SIZE-1; index++)
    {
      mu_assert(test_array[index].key <= test_array[index+1].key, "Array was not sorted");
    }

  return NULL;
}

char *test_heapsort()
{
  test_array_setup();

  struct item *sorted = merge_heap_heapsort(test_array, TEST_SIZE);
  mu_assert(sorted != NULL, "Failed to return array from heapsort.");

  int index = 0;
  for(index = 0; index < TEST_SIZE-1; index++)
    {
      mu_assert(sorted[index].key <= sorted[index+1].key, "Array was not sorted");
    }	

  free(sorted);

  return NULL;
}

/* tests are interdependant and should run in the given order */
char *all_tests()
{
  srand(time(NULL));
  test_array_setup();

  mu_suite_start();
	
  mu_run_test(makeheap);
  mu_run_test(insertion);
  mu_run_test(delmin);
  mu_run_test(test_destroy);
  mu_run_test(test_heapsort);    
  mu_run_test(test_heapsort_inplace);

  return NULL;
}

RUN_TESTS(all_tests);
