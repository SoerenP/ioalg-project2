#include <stdio.h>
#include <time.h>
#include <math.h>

#include <externalheap/minunit.h>
#include <externalheap/ext_heap.h>
#include <externalheap/io_strat_naive.h>
#include <externalheap/bool.h>

#define TEST_SIZE 128
#define BLOCK_SIZE 16
#define ELEM_SIZE sizeof(int)

static int test_array[TEST_SIZE] = {0};
static const char *filename = "tests/testfiles/naive_test.tmp";
static FILE *fp = NULL;
static struct meta *dummy_meta = NULL;

void setup_test_array()
{
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    test_array[index] = rand();

}

char *test_create_meta_ms()
{
  dummy_meta = ext_meta_create(NAIVE,BLOCK_SIZE);
  mu_assert(dummy_meta != NULL, "Failed to create naive meta struct.");

  return NULL;
}

char *test_destroy_meta_ms()
{
  int rc = ext_meta_destroy(dummy_meta);
  mu_assert(rc != EXT_ERR, "Failed to destroy meta struct.");

  return NULL;
}

char *test_open_read()
{
  fp = io_strat_naive_open(filename,EXT_MODE_R,dummy_meta);
  mu_assert(fp != NULL, "Failed to open for reading naive.");

  return NULL;
}

char *test_open_write()
{
  fp = io_strat_naive_open(filename,EXT_MODE_W,dummy_meta);
  mu_assert(fp != NULL, "Failed to open for writing naive.");

  return NULL;
}

char *test_close()
{
  int rc = io_strat_naive_close(fp,dummy_meta);
  mu_assert(rc != EXT_ERR, "Failed to close naive.");

  return NULL;
}

char *test_write_all()
{
  int index = 0;
  for(index = 0; index < ceil(TEST_SIZE / BLOCK_SIZE); index++)
    {
      int rc = io_strat_naive_write(fp,BLOCK_SIZE,ELEM_SIZE,&test_array[index * BLOCK_SIZE],dummy_meta);
      mu_assert(rc != EXT_ERR, "Failed to write naive.");
    }

  return NULL;
}

char *test_read_all()
{
  int res_array[TEST_SIZE] = {0};
  //int rc = io_strat_naive_read(fp,TEST_SIZE,ELEM_SIZE,res_array,dummy_meta);
  //mu_assert(rc != EXT_ERR, "Failed to read naive.");

  int index = 0;
  for(index = 0; index < TEST_SIZE / BLOCK_SIZE; index++)
    {
      int rc = io_strat_naive_read(fp,BLOCK_SIZE,ELEM_SIZE,&res_array[index * BLOCK_SIZE],dummy_meta);
      mu_assert(rc != EXT_ERR, "Failed to write naive.");
      mu_assert(res_array[index] == test_array[index], "Failed to read what was written naive.");
    }

  /* should already be at eof */
  mu_assert(dummy_meta->mn->eof == TRUE,"Should set EOF preemptively");

  /* try to read again, should give EXT_ERR since file at EOF */
  int rc = io_strat_naive_read(fp,BLOCK_SIZE,ELEM_SIZE,res_array,dummy_meta);
  mu_assert(rc == EXT_EOF, "Should be at EOF.");

  return NULL;
}

char *test_write_singles()
{
  int index;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int rc = io_strat_naive_write(fp,1,ELEM_SIZE,&test_array[index],dummy_meta);
      mu_assert(rc != EXT_ERR, "Failed to write single element naive.");
    }
  return NULL;
}

char *test_read_singles()
{
  int index;
  int buf;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int rc = io_strat_naive_read(fp,1,ELEM_SIZE,&buf,dummy_meta);
      mu_assert(rc != EXT_ERR, "Failed to read single element.");
      mu_assert(buf == test_array[index], "Failed to read what was written");
    }

  /* should already be EOF preemptively */
  mu_assert(dummy_meta->mn->eof == TRUE, "Should set EOF preemptively");

  /* read another element, should be at EOF */
  int rc = io_strat_naive_read(fp,1,ELEM_SIZE,&buf,dummy_meta);
  mu_assert(rc == EXT_EOF, "Should be at EOF.");

  return NULL;
}

char *test_read_with_whileloop()
{
  int index = 0;
  int buf;

  char *temp_filename = "tests/testfiles/naive_test_while.tmp";
  FILE *f = io_strat_naive_open(temp_filename,EXT_MODE_W_PLUS,dummy_meta);
  int written = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int rc = io_strat_naive_write(f,1,ELEM_SIZE,&test_array[index],dummy_meta);
      mu_assert(rc != EXT_ERR, "Failed to write");
      written++;
    }
  io_strat_naive_close(f,dummy_meta);
  debug("wrote %d\n",written);


  /* now try to read using a while loop */
  FILE *z = io_strat_naive_open(temp_filename,EXT_MODE_R_PLUS,dummy_meta);
  int counter = 0;
  while(counter < TEST_SIZE)
    {
      int rc = io_strat_naive_read(z,1,ELEM_SIZE,&buf,dummy_meta);
      if(rc <= EXT_EOF){
	debug("rc was %d, buf was %d\n",rc,buf);
	debug("is EOF set? %d\n",dummy_meta->mn->eof);
	debug("elements in buffer %d, index %d\n",dummy_meta->mn->elements_in_buffer,dummy_meta->mn->index);
	break;
      }	
      counter++;
    }
  int rc = io_strat_naive_read(z,1,ELEM_SIZE,&buf,dummy_meta);
  debug("tried to read again, buf %d, test_array[index] %d\n",buf,test_array[TEST_SIZE-1]);
  rc++;
  if(counter != TEST_SIZE) debug("read counter %d, should have read test_size %d\n",counter,TEST_SIZE);
  mu_assert(counter == TEST_SIZE, "Failed to read what was written");

  io_strat_naive_close(z,dummy_meta);
	
  return NULL;
}

char *all_tests()
{
  srand(time(NULL));
  setup_test_array();

  mu_suite_start();

  /* setup meta struct */
  mu_run_test(test_create_meta_ms);

  /* entire arrays at once */
  mu_run_test(test_open_write);
  mu_run_test(test_write_all);
  mu_run_test(test_close);
  mu_run_test(test_open_read);
  mu_run_test(test_read_all);
  mu_run_test(test_close);

  /* reset to be sure */
  setup_test_array();

  /* single elements at a time */
  mu_run_test(test_open_write);
  mu_run_test(test_write_singles);
  mu_run_test(test_close);
  mu_run_test(test_open_read);
  mu_run_test(test_read_singles);
  mu_run_test(test_close);

  /* testing with a whileloop as in the heap implementation */
  mu_run_test(test_read_with_whileloop);

  /* destroy meta structs */
  mu_run_test(test_destroy_meta_ms);

  return NULL;
}


RUN_TESTS(all_tests);
