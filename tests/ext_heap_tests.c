#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <math.h>

#include <externalheap/minunit.h>
#include <externalheap/ext_heap.h>
#include <externalheap/io_strat_std.h>
#include <externalheap/io_strat_naive.h>
#include <externalheap/io_strat_mmap.h>
#include <externalheap/util.h>
#include <externalheap/meta.h>

static struct ext_heap *heap = NULL;

#define M 8192
#define B 4096
#define TEST_SIZE 8192*4
#define HEAP_MAX_SIZE ( (int)ceil((double)TEST_SIZE/(double)M) )

#define RANGE 1000000

static int test_array[TEST_SIZE];
static int iterative_test_array[TEST_SIZE];

void setup_test_array()
{
  int index;
  for(index = 0; index < TEST_SIZE; index++)
    {
      test_array[index] = rand() % RANGE;
      iterative_test_array[index] = index;
    }
}

char *test_create()
{
  heap = ext_heap_create(HEAP_MAX_SIZE,
			 M,
			 B,
			 STD,
			 io_strat_std_write,
			 io_strat_std_open,
			 io_strat_std_read,
			 io_strat_std_close);
  mu_assert(heap != NULL, "Failed to create external heap.");
  return NULL;
}

char *test_create_naive()
{
  heap = ext_heap_create(HEAP_MAX_SIZE,
			 M,
			 B,
			 NAIVE,
			 io_strat_naive_write,
			 io_strat_naive_open,
			 io_strat_naive_read,
			 io_strat_naive_close);
  mu_assert(heap != NULL, "Failed to create external heap.");
  return NULL;
}

char *test_create_mmap()
{
  heap = ext_heap_create(HEAP_MAX_SIZE,
			 M,
			 B,
			 MMAP,
			 io_strat_mmap_write,
			 io_strat_mmap_open,
			 io_strat_mmap_read,
			 io_strat_mmap_close);
  mu_assert(heap != NULL, "Failed to create external heap.");
  return NULL;
}


char *test_insert()
{
  /* insert all values */
  int nodes = floor((double)TEST_SIZE/(double)M);
  debug("nodes; %d \n",nodes);
  int index;
  for(index = 0; index < TEST_SIZE; index++)
    {
      ext_heap_insert(heap, test_array[index]);
    }

  char name[UTIL_NAME_SIZE];
  next_file_name(name, UTIL_NAME_SIZE, UTIL_STD_NODE_PATH, TRUE);

  /* assert that each node is sorted internally */
  int elements_read = 0;
  for(index = 0; index < nodes; index++)
    {
      struct meta *m = ext_meta_create(STD, B);
      FILE *file = io_strat_std_open(name, EXT_MODE_R_PLUS, m);
      int buf;
      int old = 0;
      //debug("\t node %d\n",index);
      while(io_strat_std_read(file, 1, sizeof(int), &buf, m) > 0)
	{
	  mu_assert( old <= buf, "Not sorted order internally in node.");
	  elements_read++;
	  //debug("\t\tread %d\n",buf);
	}

      io_strat_std_close(file,m);
      ext_meta_destroy(m);
      next_file_name(name,UTIL_NAME_SIZE,UTIL_STD_NODE_PATH, FALSE);
    }
  if (elements_read != TEST_SIZE) {
    //debug("elementsread: %d, test size: %d\n", elements_read, TEST_SIZE);
    //debug("index in insert buffer: %d\n", heap->insert_buffer->idx);
  }

  if(elements_read + heap->insert_buffer->idx != TEST_SIZE){
    //debug("elementsread: %d, test size: %d\n",elements_read, TEST_SIZE);
    //debug("index in insert buffer: %d\n", heap->insert_buffer->idx);
  }
  mu_assert((elements_read + heap->insert_buffer->idx) == TEST_SIZE, "Did not retrieve all that was inserted.");

  /* assert that heap order is preserved */
  next_file_name(name, UTIL_NAME_SIZE, UTIL_STD_NODE_PATH, TRUE);
  int parent_max = 0;
  int fanout = M/B;
  int current_min = INT_MAX;
  for(index = 1; index < nodes; index++)
    {
      int parent_index = heap_parent(index,fanout);
      parent_max = heap->nodes[parent_index]->max;
      current_min = heap->nodes[index]->min;
      mu_assert(parent_max <= current_min, "Heap order violated.");
    }

  return NULL;
}


char *test_insert_naive()
{
  /* insert all values */
  int nodes = floor((double)TEST_SIZE/(double)M);
  //debug("nodes; %d \n",nodes);
  int index;
  for(index = 0; index < TEST_SIZE; index++)
    {
      ext_heap_insert(heap, test_array[index]);
    }

  char name[UTIL_NAME_SIZE];
  next_file_name(name, UTIL_NAME_SIZE, UTIL_STD_NODE_PATH, TRUE);

  /* assert that each node is sorted internally */
  int elements_read = 0;
  for(index = 0; index < nodes; index++)
    {
      struct meta *m = ext_meta_create(NAIVE, B);
      FILE *file = io_strat_naive_open(name, EXT_MODE_R_PLUS, m);
      int buf;
      int old = 0;
      //change away from tihs while loop? eof in naive is not like the others i think
      while(io_strat_naive_read(file, 1, sizeof(int), &buf, m) > 0)
	{
	  mu_assert( old <= buf, "Not sorted order internally in node.");
	  elements_read++;
	  //debug("\t\tread %d\n",buf);
	}

      io_strat_naive_close(file,m);
      ext_meta_destroy(m);
      next_file_name(name,UTIL_NAME_SIZE,UTIL_STD_NODE_PATH, FALSE);
    }
  if (elements_read != TEST_SIZE) {
    //debug("elementsread: %d, test size: %d\n", elements_read, TEST_SIZE);
    //debug("index in insert buffer: %d\n", heap->insert_buffer->idx);
  }

  if(elements_read + heap->insert_buffer->idx != TEST_SIZE){
    //debug("elementsread: %d, test size: %d\n",elements_read, TEST_SIZE);
    //debug("index in insert buffer: %d\n", heap->insert_buffer->idx);
  }
  mu_assert((elements_read + heap->insert_buffer->idx) == TEST_SIZE, "Did not retrieve all that was inserted.");

  /* assert that heap order is preserved */
  next_file_name(name, UTIL_NAME_SIZE, UTIL_STD_NODE_PATH, TRUE);
  int parent_max = 0;
  int fanout = M/B;
  int current_min = INT_MAX;
  for(index = 1; index < nodes; index++)
    {
      int parent_index = heap_parent(index,fanout);
      parent_max = heap->nodes[parent_index]->max;
      current_min = heap->nodes[index]->min;
      mu_assert(parent_max <= current_min, "Heap order violated.");
    }

  return NULL;
}

char *test_delete_min()
{
  int index = 0;
  int i;
  int totalsize = TEST_SIZE;

  //for (i = 0; i < heap->size; i++)
  //printfile(heap->nodes[i]->filename,B);
  
  //for (i = 0; i < heap->size; i++)
  //print_node(heap->nodes[i]);
  //debug("-----------------------------------------------------\n deletemin: \n");
  //debug("before delete");
  //for (i = 0; i < heap->size; i++)
  //print_node(heap->nodes[i]);

  int old_deleted_minimum = ext_heap_delete_min(heap);
  //debug("\t\t deleted: %d, iter (%d/%d)\n",old_deleted_minimum,index+1,TEST_SIZE);
  totalsize--;
  //debug("after delete");
  for (i = 0; i < heap->size; i++)
    print_node(heap->nodes[i]);
  for(index = 1; index < TEST_SIZE; index++)
    {
      debug("-----------------------------------------------------\n deletemin: \n");
      int deleted_minimum = ext_heap_delete_min(heap);
    
      //debug("\t\t deleted: %d, iter (%d/%d), heapsize %d, insertbuffer: %d\n",deleted_minimum,index+1,TEST_SIZE,heap->size, heap->insert_buffer->idx);
      totalsize--;
    
      int actualtotalsize = count_elements_in_heap(heap);
      if (actualtotalsize != totalsize) {
	//debug("total size: %d, actual: %d\n", totalsize, actualtotalsize);
      	//printf("total size: %d, actual: %d\n",totalsize,actualtotalsize);
	for (i = 0; i < heap->size; i++){
          print_node(heap->nodes[i]);
        }
      }
      mu_assert(totalsize == actualtotalsize, "Total size violation.");
    
      // check everything except lastleaf
      for (i = 0; i < heap->size-1; i++){
	struct ext_node *n = heap->nodes[i];
	if ((n->num_elements - n->deleted) < (M / 2)){
	  for (i = 0; i < heap->size; i++){
	    print_node(heap->nodes[i]);
	  }
	}
	mu_assert((n->num_elements - n->deleted) >= (M / 2) -1, "Violation of num - deleted");
	mu_assert(n->num_elements >= 0, "Number of elements was less than 0");
      }
    
      if (!(old_deleted_minimum <= deleted_minimum)){
	for (i = 0; i < heap->size; i++)
	  print_node(heap->nodes[i]);
      }
      if(!(old_deleted_minimum <= deleted_minimum)) debug("old_deleted_minimum %d <= deleted_minimum %d\n",old_deleted_minimum,deleted_minimum);
      mu_assert(old_deleted_minimum <= deleted_minimum, "Elements should be extracted in sorted order");
      old_deleted_minimum = deleted_minimum;
    }

  return NULL;
}

char *test_delete_min_naive()
{
  int index = 0;
  int i;
  int totalsize = TEST_SIZE;

  //for (i = 0; i < heap->size; i++)
  // printfile(heap->nodes[i]->filename,B);
  
  // for (i = 0; i < heap->size; i++)
  //  print_node(heap->nodes[i]);
  //debug("-----------------------------------------------------\n deletemin: \n");
  //debug("before delete");
  // for (i = 0; i < heap->size; i++)
  // print_node(heap->nodes[i]);

  int old_deleted_minimum = ext_heap_delete_min(heap);
  //debug("\t\t deleted: %d, iter (%d/%d)\n",old_deleted_minimum,index+1,TEST_SIZE);
  totalsize--;
  //debug("after delete");
  //for (i = 0; i < heap->size; i++)
  // print_node(heap->nodes[i]);
  for(index = 1; index < TEST_SIZE; index++)
    {
      //debug("-----------------------------------------------------\n deletemin: \n");
      int deleted_minimum = ext_heap_delete_min(heap);
    
      //debug("\t\t deleted: %d, iter (%d/%d), heapsize %d, insertbuffer: %d\n",deleted_minimum,index+1,TEST_SIZE,heap->size, heap->insert_buffer->idx);
      totalsize--;
    
      int actualtotalsize = count_elements_in_heap(heap);
      if (actualtotalsize != totalsize) {
	//debug("total size: %d, actual: %d\n", totalsize, actualtotalsize);
	//	printf("total size: %d, actual: %d\n",totalsize,actualtotalsize);
	for (i = 0; i < heap->size; i++){
          print_node(heap->nodes[i]);
        }
      }
      mu_assert(totalsize == actualtotalsize, "Total size violation.");
    
      // check everything except lastleaf
      for (i = 0; i < heap->size-1; i++){
	struct ext_node *n = heap->nodes[i];
	if ((n->num_elements - n->deleted) < (M / 2)){
	  for (i = 0; i < heap->size; i++){
	    print_node(heap->nodes[i]);
	  }
	}
	mu_assert((n->num_elements - n->deleted) >= (M / 2) -1, "Violation of num - deleted");
	mu_assert(n->num_elements >= 0, "Number of elements was less than 0");
      }
    
      if (!(old_deleted_minimum <= deleted_minimum)){
	for (i = 0; i < heap->size; i++)
	  print_node(heap->nodes[i]);
      }
      if(!(old_deleted_minimum <= deleted_minimum)) debug("old_deleted_minimum %d <= deleted_minimum %d\n",old_deleted_minimum,deleted_minimum);
      mu_assert(old_deleted_minimum <= deleted_minimum, "Elements should be extracted in sorted order");
      old_deleted_minimum = deleted_minimum;
    }

  return NULL;
}

char *test_delete_min_iterative_array()
{
  //debug("==============================================================\n");
  //debug("delete_min_iterative START\n");
  struct ext_heap *Q =
    ext_heap_create(HEAP_MAX_SIZE,
                    M,
                    B,
                    STD,
                    io_strat_std_write,
                    io_strat_std_open,
                    io_strat_std_read,
                    io_strat_std_close);

  int index;
  for(index = 0; index < TEST_SIZE; index++){
    ext_heap_insert(Q, iterative_test_array[index]);
  }

  index = 0;
  int i;
  //debug("-----------------------------------------------------\n deletemin: \n");
  int old_deleted_minimum = ext_heap_delete_min(Q);
  //debug("\t\t deleted: %d, iter (%d/%d)\n", old_deleted_minimum, index+1, TEST_SIZE);
  
  for (i = 0; i < Q->size; i++)
    print_node(Q->nodes[i]);
  
  for(index = 1; index < TEST_SIZE; index++){
    //debug("-----------------------------------------------------\n deletemin: \n");
    int deleted_minimum = ext_heap_delete_min(Q);
    //debug("\t\t deleted: %d, iter (%d/%d), heapsize %d\n", deleted_minimum, index+1, TEST_SIZE, Q->size);

    if (!(old_deleted_minimum <= deleted_minimum)){
      for (i = 0; i < Q->size; i++)
	print_node(Q->nodes[i]);
    }
    
    mu_assert(old_deleted_minimum <= deleted_minimum, "Elements should be extracted in sorted order");
    old_deleted_minimum = deleted_minimum;
  }
  //debug("==============================================================\n");

  ext_heap_destroy(Q);
  
  return NULL;
}

char *test_destroy()
{
  ext_heap_destroy(heap);
  return NULL;
}

char *test_heapsort_std_strat()
{
  char input_name[UTIL_NAME_SIZE];
  char output_name[UTIL_NAME_SIZE];
  next_file_name(input_name, UTIL_NAME_SIZE, UTIL_STD_SOURCENODE_PATH,TRUE);
  next_file_name(output_name, UTIL_NAME_SIZE, UTIL_STD_SOURCENODE_PATH,FALSE);

  /* create input file */
  struct ext_node *input = ext_node_create(input_name,TEST_SIZE,sizeof(int),STD,B);
  mu_assert(input != NULL, "Failed to create input");

  /* fill it up man! */
  FILE *file = io_strat_std_open(input_name,EXT_MODE_W_PLUS,input->m);
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int r = rand();
      io_strat_std_write(file,1,sizeof(int),&r,input->m);
    }
  io_strat_std_close(file,input->m);

  /* create the output file */
  struct ext_node *output = ext_node_create(output_name,TEST_SIZE,sizeof(int),STD,B);
  mu_assert(input != NULL, "Failed to create output");

  /* call heapsort */
  int rc = ext_heap_sort(input, output, TEST_SIZE,M,B,STD,io_strat_std_write,io_strat_std_open,io_strat_std_read,io_strat_std_close);
  mu_assert(rc != EXT_ERR, "Failed to sort.");

  /* check if everything in the resulting file is sorted */
  FILE *res = io_strat_std_open(output_name,EXT_MODE_R_PLUS,output->m);
  int old_min = -1;
  int buf = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      rc = io_strat_std_read(res,1,sizeof(int),&buf,output->m);
      mu_assert(buf >= old_min, "Failed to retrieve in sorted order");
      old_min = buf;
    }

  io_strat_std_close(res,output->m);

  ext_node_destroy(input);
  ext_node_destroy(output);

  return NULL;
}

char *test_heapsort_mmap_strat()
{
  char input_name[UTIL_NAME_SIZE];
  char output_name[UTIL_NAME_SIZE];
  next_file_name(input_name, UTIL_NAME_SIZE, UTIL_STD_SOURCENODE_PATH,TRUE);
  next_file_name(output_name, UTIL_NAME_SIZE, UTIL_STD_SOURCENODE_PATH,FALSE);

  /* create input file */
  struct ext_node *input = ext_node_create(input_name,TEST_SIZE,sizeof(int),MMAP,B);
  mu_assert(input != NULL, "Failed to create input");

  /* fill it up man! */
  FILE *file = io_strat_mmap_open(input_name,EXT_MODE_W_PLUS,input->m);
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int r = rand();
      io_strat_mmap_write(file,1,sizeof(int),&r,input->m);
    }
  io_strat_mmap_close(file,input->m);

  /* create the output file */
  struct ext_node *output = ext_node_create(output_name,TEST_SIZE,sizeof(int),MMAP,B);
  mu_assert(input != NULL, "Failed to create output");

  /* call heapsort */
  int rc = ext_heap_sort(input, output, TEST_SIZE,M,B,MMAP,io_strat_mmap_write,io_strat_mmap_open,io_strat_mmap_read,io_strat_mmap_close);
  mu_assert(rc != EXT_ERR, "Failed to sort.");

  /* check if everything in the resulting file is sorted */
  FILE *res = io_strat_mmap_open(output_name,EXT_MODE_R_PLUS,output->m);
  int old_min = -1;
  int buf = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      rc = io_strat_mmap_read(res,1,sizeof(int),&buf,output->m);
      mu_assert(buf >= old_min, "Failed to retrieve in sorted order");
      old_min = buf;
    }

  io_strat_mmap_close(res,output->m);

  ext_node_destroy(input);
  ext_node_destroy(output);

  return NULL;
}

char *test_heapsort_naive_strat()
{
  char input_name[UTIL_NAME_SIZE];
  char output_name[UTIL_NAME_SIZE];
  next_file_name(input_name, UTIL_NAME_SIZE, UTIL_STD_SOURCENODE_PATH,TRUE);
  next_file_name(output_name, UTIL_NAME_SIZE, UTIL_STD_SOURCENODE_PATH,FALSE);

  /* create input file */
  struct ext_node *input = ext_node_create(input_name,TEST_SIZE,sizeof(int),NAIVE,B);
  mu_assert(input != NULL, "Failed to create input");

  /* fill it up man! */
  FILE *file = io_strat_mmap_open(input_name,EXT_MODE_W_PLUS,input->m);
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int r = rand();
      io_strat_naive_write(file,1,sizeof(int),&r,input->m);
    }
  io_strat_naive_close(file,input->m);

  /* create the output file */
  struct ext_node *output = ext_node_create(output_name,TEST_SIZE,sizeof(int),NAIVE,B);
  mu_assert(input != NULL, "Failed to create output");

  /* call heapsort */
  int rc = ext_heap_sort(input, output, TEST_SIZE,M,B,NAIVE,io_strat_naive_write,io_strat_naive_open,io_strat_naive_read,io_strat_naive_close);
  mu_assert(rc != EXT_ERR, "Failed to sort.");

  /* check if everything in the resulting file is sorted */
  FILE *res = io_strat_naive_open(output_name,EXT_MODE_R_PLUS,output->m);
  int old_min = -1;
  int buf = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      rc = io_strat_naive_read(res,1,sizeof(int),&buf,output->m);
      mu_assert(buf >= old_min, "Failed to retrieve in sorted order");
      old_min = buf;
    }

  io_strat_naive_close(res,output->m);

  ext_node_destroy(input);
  ext_node_destroy(output);

  return NULL;
}


char *all_tests()
{

  srand(time(NULL));

  setup_test_array();

  mu_suite_start();

  // std
  mu_run_test(test_create);
  mu_run_test(test_insert);
  mu_run_test(test_delete_min);
  mu_run_test(test_destroy);

  // naive
  mu_run_test(test_create_naive);
  mu_run_test(test_insert_naive);
  mu_run_test(test_delete_min_naive);
  mu_run_test(test_destroy);
 
  // mmap
  mu_run_test(test_create_mmap);
  mu_run_test(test_insert);
  mu_run_test(test_delete_min);
  mu_run_test(test_destroy);

  // other stuff
  mu_run_test(test_delete_min_iterative_array);

  //heapsort
  mu_run_test(test_heapsort_std_strat);
  mu_run_test(test_heapsort_mmap_strat);
  mu_run_test(test_heapsort_naive_strat);

  return NULL;
}

RUN_TESTS(all_tests);
