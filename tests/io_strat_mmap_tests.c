#include <stdio.h>
#include <time.h>

#include <externalheap/minunit.h>
#include <externalheap/meta.h>
#include <externalheap/ext_heap.h>
#include <externalheap/io_strat_mmap.h>
#include <externalheap/io_strat_naive.h>

#define TEST_SIZE 10000
#define ELEM_SIZE sizeof(int)

static int test_array[TEST_SIZE] = {0};
static const char *filename = "tests/testfiles/mmap_test.tmp";
static FILE *fp = NULL;
static struct meta *reading_meta;
static struct meta *writing_meta;

void setup_metas(){
  reading_meta = ext_meta_create(MMAP, 4096);
  writing_meta = ext_meta_create(MMAP, 4096);
}

void destroy_metas(){
  ext_meta_destroy(reading_meta);
  ext_meta_destroy(writing_meta);
}

void setup_test_array()
{
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    test_array[index] = rand();
}

char *test_open_read()
{
  fp = io_strat_mmap_open(filename, EXT_MODE_R_PLUS, reading_meta);
  mu_assert(fp != NULL, "Failed to open for reading mmap.");

  return NULL;
}

char *test_open_write()
{
  fp = io_strat_mmap_open(filename, EXT_MODE_W_PLUS, writing_meta);
  mu_assert(fp != NULL, "Failed to open for writing mmap.");

  return NULL;
}

char *test_close_write(){
  int rc = io_strat_mmap_close(fp,writing_meta);
  mu_assert(rc != EXT_ERR, "Failed to close mmap.");

  return NULL;
}

char *test_close_read()
{
  int rc = io_strat_mmap_close(fp,reading_meta);
  mu_assert(rc != EXT_ERR, "Failed to close mmap.");

  return NULL;
}

char *test_write_all()
{
  int rc = io_strat_mmap_write(fp, TEST_SIZE, ELEM_SIZE, test_array, writing_meta);
  mu_assert(rc != EXT_ERR, "Failed to write mmap.");
  return NULL;
}

char *test_read_all()
{
  int *res_array = malloc(TEST_SIZE*ELEM_SIZE);
  int rc = io_strat_mmap_read(fp, TEST_SIZE, ELEM_SIZE, res_array, reading_meta);
  mu_assert(rc != EXT_ERR, "Failed to read mmap.");
  

  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      fprintf(stderr, "got : %d, but expected %d  \n", res_array[index], test_array[index]);
      mu_assert(res_array[index] == test_array[index], "Failed to read what was written mmap.");
    }

  free(res_array);
  return NULL;
}

char *test_write_singles()
{
  int index;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int rc = io_strat_mmap_write(fp, 1, ELEM_SIZE, &test_array[index], writing_meta);
      mu_assert(rc != EXT_ERR, "Failed to write single element mmap.");
    }
  return NULL;
}

char *test_read_singles()
{
  int index = 0;
  int buf = 0;
  for(index = 0; index < TEST_SIZE; index++)
    {
      int rc = io_strat_mmap_read(fp, 1, ELEM_SIZE, &buf, reading_meta);
      mu_assert(rc != EXT_ERR, "Failed to read single element.");
      mu_assert(buf == test_array[index], "Failed to read what was written");
    }
  return NULL;
}

char *all_tests()
{
  srand(time(NULL));
  setup_test_array();
  setup_metas();

  mu_suite_start();

  /* entire arrays at once */
  mu_run_test(test_open_write);
  mu_run_test(test_write_all);
  mu_run_test(test_close_write);

  mu_run_test(test_open_read);
  mu_run_test(test_read_all);
  mu_run_test(test_close_read);

  /* reset to be sure */
  destroy_metas();
  setup_metas();
  setup_test_array();
  filename = "tests/testfiles/mmap_test2.tmp";

  /* single elements at a time */
  mu_run_test(test_open_write);
  mu_run_test(test_write_singles);
  mu_run_test(test_close_write);

  mu_run_test(test_open_read);
  mu_run_test(test_read_singles);
  mu_run_test(test_close_read);

  destroy_metas();

  return NULL;
}


RUN_TESTS(all_tests);
