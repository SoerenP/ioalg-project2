#include <externalheap/meta.h>
#include <externalheap/ext_heap.h>
#include <externalheap/dbg.h>
#include <externalheap/io_strat_std.h>
#include <externalheap/io_strat_naive.h>
#include <externalheap/io_strat_mmap.h>
#include <externalheap/util.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define EXPECTED_ARGS 5

int main(int argc, char ** argv){
  if (argc != EXPECTED_ARGS){
    printf("Usage: %s N M B Strategy(0:Naive, 1:STD, 2:MMAP) \n",argv[0]);
    exit(1);
  }

  int N = atoi(argv[1]);
  int M = atoi(argv[2]);
  int B = atoi(argv[3]);
  int strat = atoi(argv[4]);

  char input_name[UTIL_NAME_SIZE];
  char output_name[UTIL_NAME_SIZE];
  next_file_name(input_name, UTIL_NAME_SIZE, UTIL_STD_SOURCENODE_PATH,TRUE);
  next_file_name(output_name, UTIL_NAME_SIZE, UTIL_STD_SOURCENODE_PATH,FALSE);

  srand(time(NULL));
  struct ext_node *input;
  struct ext_node *output;

  if (strat == 0){
    input = ext_node_create(input_name, N, sizeof(int), NAIVE, B);
    output = ext_node_create(output_name, N, sizeof(int), NAIVE, B);
  } else if (strat == 1) {
    input = ext_node_create(input_name, N, sizeof(int), STD, B);
    output = ext_node_create(output_name, N, sizeof(int), STD, B);
  } else if (strat == 2){
    input = ext_node_create(input_name, N, sizeof(int), MMAP, B);
    output = ext_node_create(output_name, N, sizeof(int), MMAP, B);
  } else {
    printf("Wrong strategy\n");
    exit(2);
  }
  
  // fill the input with random numbers
  int i;
  int buf = 0;
  struct meta *m = ext_meta_create(STD, B);
  FILE *f = (*io_strat_std_open)(input_name, EXT_MODE_W_PLUS, m);
  for (i = 0; i < N; i++){
    buf = rand();
    (*io_strat_std_write)(f, 1, sizeof(int), &buf, m);
  }
  (*io_strat_std_close)(f, m);

  clock_t begin, end;
  double time_spent;

  begin = clock();

  if (strat == 0){
    ext_heap_sort(input, output, N, M, B,
                  NAIVE,
                  io_strat_naive_write,
                  io_strat_naive_open,
                  io_strat_naive_read,
                  io_strat_naive_close);
  } else if (strat == 1) {
    ext_heap_sort(input, output, N, M, B,
                  STD,
                  io_strat_std_write,
                  io_strat_std_open,
                  io_strat_std_read,
                  io_strat_std_close);
  } else if (strat == 2){
    ext_heap_sort(input, output, N, M, B,
                  MMAP,
                  io_strat_mmap_write,
                  io_strat_mmap_open,
                  io_strat_mmap_read,
                  io_strat_mmap_close);
  } else {
    printf("Wrong strategy\n");
    exit(2);
  }

  end = clock();

  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("%d \t%d \t%d \t%d \t%f \t%f\n", N, M, B, strat, time_spent, ((double)M/(double)B));

  ext_node_destroy(input);
  ext_node_destroy(output);
  ext_meta_destroy(m);
  
  return 0;
}


