set title "Fixed M, Fixed B, varying N, logscale xy"
set xlabel "N"
set ylabel "Time (s)"

set logscale xy
set log xy 2
set format x "2^{%L}"

set grid
set style data linespoints
set key outside center bottom
set key autotitle columnhead
set terminal png size 600,500

set output "experiments/pngs/lock-m-b-vary-n.png"
# n m b s time
plot  "experiments/results/lock-m-b-vary-n-s-0.gdata" using 1:5 title "Naive",\
      "experiments/results/lock-m-b-vary-n-s-1.gdata" using 1:5 title "STD",\
      "experiments/results/lock-m-b-vary-n-s-2.gdata" using 1:5 title "MMAP",\
      "experiments/results/heapsort.gdata"  using 1:2 title "Int-Heapsort",\
      "experiments/results/quicksort.gdata" using 1:2 title "Int-Quicksort",\
      "experiments/results/mergesort.gdata" using 1:4 title "Ext-Mergesort"