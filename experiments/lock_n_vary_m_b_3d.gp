set title "Fixed N, Varying M, varying B"
set xlabel "B"
set ylabel "M"
set zlabel "Time (s)"

set logscale xy
set log xy 2
set format xy "2^{%L}"

set dgrid3d
set style data linespoints
set key outside center bottom
set key autotitle columnhead
set terminal png size 800,500

set output "experiments/pngs/lock-n-vary-m-b.png"
# n m b s time
splot "experiments/results/lock-n-vary-m-b-s-0.gdata" using 3:2:5 title "Naive",\
      "experiments/results/lock-n-vary-m-b-s-1.gdata" using 3:2:5 title "STD",\
      "experiments/results/lock-n-vary-m-b-s-2.gdata" using 3:2:5 title "MMAP"