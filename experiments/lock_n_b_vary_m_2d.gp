set title "Fixed N, Fixed B, varying M, logscale x"
set xlabel "M"
set ylabel "Time (s)"

set logscale x
set log x 2
set format x "2^{%L}"

set grid
set style data linespoints
set key outside center bottom
set key autotitle columnhead
set terminal png size 600,500

set output "experiments/pngs/lock-n-b-vary-m.png"
# n m b s time
plot  "experiments/results/lock-n-b-vary-m-s-0.gdata" using 2:5 title "Naive",\
      "experiments/results/lock-n-b-vary-m-s-1.gdata" using 2:5 title "STD",\
      "experiments/results/lock-n-b-vary-m-s-2.gdata" using 2:5 title "MMAP"