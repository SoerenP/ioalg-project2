CFLAGS=-g -Wall -Wextra -Isrc -rdynamic $(OPTFLAGS) -DNDEBUG -O0 #-O2
LDLIBS= -ldl $(OPTLIBS)
PREFIX?=/usr/local 

SOURCES=$(wildcard src/**/*.c src/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))

TEST_SRC=$(wildcard tests/*_tests.c)
TESTS=$(patsubst %.c,%,$(TEST_SRC))

PROGRAM_SRC=$(wildcard bin/*.c)
PROGRAMS=$(patsubst %.c,%,$(PROGRAMS_SRC))

EXPERIMENTS_SRC=$(wildcard experiments/*.c)
EXPERIMENTS=$(patsubst %.c,%,$(EXPERIMENTS_SRC))

TARGET=build/libexternalheap.a 
SO_TARGET=$(patsubst %.a,%.so,$(TARGET))

# The target build
all: $(TARGET) tests programs experiments

dev: CFLAGS=-g -Wall -Isrc -Wall -Wextra $(OPTFLAGS)
dev: all

$(TARGET): CFLAGS += -fPIC
$(TARGET): build $(OBJECTS) $(LDLIBS)
	ar rcs $@ $(OBJECTS)
	ranlib $@

$(SO_TARGET): $(TARGET) $(OBJECTS)
	$(CC) -shared -o $@ $(OBJECTS)

$(PROGRAMS): CFLAGS += $(TARGET)

$(EXPERIMENTS): CFLAGS += $(TARGET)

build:
	@mkdir -p build
	@mkdir -p bin
	@mkdir -p experiments

# The programs
.PHONY: programs
programs: LDLIBS += -L./build -lexternalheap -lm

# The Unit Tests
.PHONY: tests
tests: LDLIBS += -L./build -lexternalheap -lm
tests: $(TESTS)
	sh ./tests/runtests.sh

# The experiments
.PHONY: experiments
experiments: LDLIBS += -L./build -lexternalheap -lm
experiemtns: LDLIBS += -L./build -lIOAlg -lm
experiments: $(EXPERIMENTS)

valgrind:
	VALGRIND="valgrind --log-file=/tmp/valgrind-%p.log" $(MAKE)

#The cleaner
clean:
	rm -rf build $(OBJECTS) $(PROGRAMS) $(EXPERIMENTS) $(TESTS) 
	rm -f tests/tests.log
	find . -name "*.gc*" -exec rm {} \;
	rm -f `find . -name "*.dSYM" -print`

#The install
install: all
	install -d $(DESTDIR)/$(PREFIX)/lib/
	install $(TARGET) $(DESTDIR)/$(PREFIX)/lib/

#The checker
BADFUNCS='[^_-a-zA-Z0-9](str(n?cpy|n?cat|xfrm|n?dup|str|pbrk|tok|_)|stpn?cpy|a?sn?printf|byte_)'
check:
	@echo Files with potentially dangerous functions
	@egrep $(BADFUNCS) $(SOURCES) || true 
